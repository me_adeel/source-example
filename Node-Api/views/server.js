var cluster  = require('cluster'), _portSocket  = 8080, _portRedis   = 6379, _HostRedis   = 'localhost';
var express = require('express');
var path = require('path');
var http = require('http');
var server = http.createServer(app).listen();
var socketIO = require('socket.io').listen(server);

 var redis = require('socket.io-redis');

if (cluster.isMaster) { 
  
  socketIO.adapter(redis({ host: _HostRedis, port: _portRedis }));
  
  var numberOfCPUs = require('os').cpus().length;
  for (var i = 0; i < numberOfCPUs; i++) {
    cluster.fork();   
  }
  
    cluster.on('fork', function(worker) {
        console.log('Worker %s created', worker.id);
    });
    cluster.on('online', function(worker) {
         console.log('Worker %s online', worker.id);
    });
    cluster.on('listening', function(worker, addr) {
        console.log('Worker %s listening on %s:%d', worker.id, addr.address, addr.port);
    });
    cluster.on('disconnect', function(worker) {
        console.log('Worker %s disconnected', worker.id);
    });
    cluster.on('exit', function(worker, code, signal) {
        console.log('Worker %s dead (%s)', worker.id, signal || code);
        if (!worker.suicide) {
            console.log('new Worker %s created', worker.id);
            cluster.fork();
        }
    });
}

if (cluster.isWorker) { 

  server = http.createServer(app).listen(_portSocket);
  
  http.globalAgent.maxSockets = Infinity; 
  
  var app = express(), ent = require('ent'), fs  = require('fs');

  app.use("/public", express.static(path.join(__dirname, 'public')));

  socketIO.adapter(redis({ host: _HostRedis, port: _portRedis }));
  
  // app.get('/', function (req, res) { 

  //   // res.emitfile(__dirname + '/interface.php');

  // });
  
  socketIO.sockets.on('connection', function(socket, pseudo) {

    // socket.setNoDelay(true);
    
    socket.on('new_client', function(pseudo) {
      pseudo = ent.encode(pseudo);      
      socket.pseudo = pseudo;
      try {
        socket.broadcast.to(socket.room).emit('new_client', pseudo);
      } catch(e) {
        socket.to(socket.room).emit('new_client', pseudo);
      }
      console.log('L\'user : '+socket.pseudo+' s\'est connecter');
    }); 

    socket.on('message', function(data) {
      socket.broadcast.to(socket.room).emit('dispatch', data);
    }); 

    socket.on('exit', function(data) { socket.close();});
    
    socket.on('room', function(newroom) {
      socket.room = newroom;
      socket.join(newroom); 
      console.log('Le membre '+socket.pseudo+' a rejoint le domaine '+socket.room);
      socket.broadcast.to(socket.room).emit('dispatch', 'L\'utilisateur : '+socket.pseudo+' a rejoint le domaine : '+socket.room);
    });
  }); 
}