var express = require('express');

module.exports = function(app){
	// register route controllers
	var main = require('../routes/l2f');
	var auth = require('../routes/auth');

	app.use('/l2f', main);
	app.use('/auth', auth);

	app.get('/',function(req,res){
			res.redirect("/view");
	});	

};