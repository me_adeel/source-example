var express = require('express');
var logger = require('morgan');
var path = require('path');
var compress = require('compression');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
  var cookieSession = require('cookie-session');


module.exports = function(app, envConfig){

	//app.use(favicon(envConfig.rootPath + '/public/favicon.ico'));
	app.use(compress());  
	app.use(logger('dev'));
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({
	  extended: true
	}));
	// app.use(bodyParser({uploadDir:'/path/to/temporary/directory/to/store/uploaded/files'}));
   var session=cookieSession({
      name: "session",
      secret: "l2fApp",
      maxAge: 15724800000
    });
    
    app.use(session);
	
	app.use(express.static(path.join(envConfig.rootPath, 'public')));
};