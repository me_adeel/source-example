var mongoose = require('mongoose'), Schema = mongoose.Schema;

var UsersSchema = new Schema({
  name: String,
  password: String,
  email: {type:String,unique:true},
  verified:Number,
  pic:String,
  fbId:String,
  coins:Number
});


//Payment 
var PaymentSchema=new Schema({
  platform:String,
  transcation:String,
  payment:String,
  packageType:String
});

//Hint and Mover


/*
Status:
0- Sent
1- Approved
2- Rejected
*/

/*
actionBy:
0- Bot
*/

var GameSchema = new Schema({
  gridSize:Number,
  user1:String,
  user2:String,
  score1:Number,
  score2:Number,
  status:Number,
  Moves:[{row:Number,col:Number,letter:String,actionBy:String}]
});

/*
Status:
0- Sent
1- Approved
2- Rejected
*/

var FriendsSchema = new Schema({
  userId:String,
  friendId:String,
  status:Number
});



var LeaderBoardScheame = new Schema({
  gameId:String,
  userId:String,
  gridSize:String,
  score:Number
});



//Chat 
var ChatSchema= new Schema({
  gameId:String,
  from:String,
  message:String,
  message_at: { type: Date }
});




mongoose.model('Users', UsersSchema);
mongoose.model('Games', GameSchema);
mongoose.model('Friends', FriendsSchema);
mongoose.model('LeaderBoard', LeaderBoardScheame);
mongoose.model('Chat', ChatSchema);
mongoose.model('Payment', PaymentSchema);

