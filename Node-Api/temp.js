// Setup basic express server
var async = require('async');
var express = require('express');
var path = require('path');
var io;
var port = 3001;
var userdata = {};
var sockets = {};
var games = {};
var numUsers = 0;
var functions=require('./routes/functions');


var net = require('net'),
    cluster = require('cluster');

var env = process.env.NODE_ENV || 'development';
var envConfig = require('./config/env')[env];

function hash(ip, seed) {
    var hash = ip.reduce(function(r, num) {
        r += parseInt(num, 10);
        r %= 2147483648;
        r += (r << 10)
        r %= 2147483648;
        r ^= r >> 6;
        return r;
    }, seed);

    hash += hash << 3;
    hash %= 2147483648;
    hash ^= hash >> 11;
    hash += hash << 15;
    hash %= 2147483648;

    return hash >>> 0;
}


var server;

// `num` argument is optional
if (typeof num !== 'number')
num = require('os').cpus().length - 1;

// Master will spawn `num` workers
if (cluster.isMaster) {
    var workers = [];
    for (var i = 0; i < num; i++) {
        ! function spawn(i) {
            workers[i] = cluster.fork();
            // Restart worker on exit
            workers[i].on('exit', function() {
                console.error('sticky-session: worker died');
                spawn(i);
            });
        }(i);
    }

    var seed = ~~(Math.random() * 1e9);
    server = net.createServer(function(c) {
        // Get int31 hash of ip
        var worker,
            ipHash = hash((c.remoteAddress || '').split(/\./g), seed);
        // console.log(c.remoteAddress);
        // Pass connection to worker
        worker = workers[ipHash % workers.length];
        worker.send('sticky-session:connection', c);
    }).listen(port);
} else {
    startServer();
}



function doStickyClusterStuff() {
    // Worker process
    process.on('message', function(msg, socket) {
        if (msg !== 'sticky-session:connection') return;

        server.emit('connection', socket);
    });

    if (!server) throw new Error('Worker hasn\'t created server!');

    // Monkey patch server to do not bind to port
    var oldListen = server.listen;
    server.listen = function listen() {
        var lastArg = arguments[arguments.length - 1];

        if (typeof lastArg === 'function') lastArg();

        return oldListen.call(this, null);
    };
}


function startServer() {
    var app = new express();
    require('./config/config')(app, envConfig);
    require('./config/database')(envConfig)
    require('./config/routes')(app);

    app.use("/public", express.static(path.join(__dirname, 'public')));
    // Don't expose our internal server to the outside.
    server = app.listen(0, 'localhost'),
    io = require('socket.io')(server);

    doStickyClusterStuff();

    console.log("listening to port " + port);
    io.adapter(require('socket.io-redis')({
        host: 'localhost',
        port: 6379
    }));

    io.on('connection', function(socket) {
        console.log("new connection");
        sockets[socket.id] = socket;

        // console.log("\nadded soccketId" + socket.id + " socket size " + Object.keys(sockets).length);

        socket.on('user',function(data){
            userdata[socket.id]={};
            userdata[socket.id]['uid']=data;
            console.log(userdata);
        });

        socket.on('game',function(data){
            socket.join(data);
            games[socket.id]=data;
            console.log(games);
        });

        socket.on('move',function(data){
            socket.broadcast.to(games[socket.id]).emit("moved","asdasd");
        });


        socket.on("send",function(data){
            io.sockets.connected[data].emit("greeting", "Howdy, User 1!"); 
        });

        socket.on('disconnect', function() {
            console.log("socket disconnect");
            delete sockets[socket.id];
            delete userdata[socket.id];
            socket.leave(games[socket.id]);
            delete games[socket.id];
        });

        socket.emit('message', { hello: 'world',id:socket.id});


    });
}