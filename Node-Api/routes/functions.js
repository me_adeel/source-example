var express = require('express')
var router = express.Router();
var mongoose = require('mongoose')
var Users=mongoose.model('Users');
var Friends=mongoose.model('Friends');
var Games=mongoose.model('Games');
var Chat=mongoose.model('Chat');
var LeaderBoard=mongoose.model('LeaderBoard');
var  path = require('path'),fs = require('fs');
var ObjectId = require('mongoose').Types.ObjectId; 
var passwordHash = require('password-hash');
var dic = require('../routes/dictionary');
var dictionary = dic.dictionary();
var LettersScore={"A":2,"B":4,"C":4,"D":3,"E":2,"F":5,"G":3,"H":5,"I":2,"J":9,"K":6,"L":2,"M":4,"N":2,"O":2,"P":4,"Q": 1,"R":2,"S":2,"T":2,"U":2,"V":5,"W":5,"X":9,"Y": 5,"Z": 11 };


function findScore(word){
  word=word.toUpperCase();
  var score=0;
  var res = word.split('');
  for(var i=0;i<res.length;i++){
    score+=LettersScore[res[i]];
  } 
  return score;
}


function searchWord (val,searchVal) {
  for (var i=0 ; i < dictionary[val].length ; i++)
  {
      if (dictionary[val][i] == searchVal.toLowerCase()) {
        return true;
      }
  }
  return false;
}


function calculateScore(gridLength,Place){
  var Search = [],UserScore=0;
  for (var i = 0; i < gridLength; i++) {
            for (var j = 0; j < gridLength - 2; j++) {
                word = '';
                for (var k = 0; k < gridLength - j; k++) {
                  var id = Place[i][j+k];
                    word += id;
                    if (word.length > gridLength-2) {
                        Search.push(word);
                    }
                }
            }
        }

  for (var i = 0; i < gridLength; i++) {
      for (var j = 0; j < gridLength - 2; j++) {
          word = '';
          for (var k = 0; k < gridLength - j; k++) {
                  var id = Place[j+k][i];
                  word += id;
                  if (word.length > gridLength-2) {
                      Search.push(word);
                  }
          }
      }
  }

   for(var i=0;i<Search.length;i++){
      var column=Search[i][0].toUpperCase()+Search[i].length; 
      var variableCheck=searchWord(column,Search[i]);

      if(variableCheck){
        var a=findScore(Search[i]);
        UserScore+=a;      
      }
  }

  return UserScore;
}

function isValidObjectID(str) {
  // coerce to string so the function can be generically used to test both strings and native objectIds created by the driver
  str = str + '';
  var len = str.length, valid = false;
  if (len == 12 || len == 24) {
    valid = /^[0-9a-fA-F]+$/.test(str);
  }
  return valid;
}


module.exports = {
  
  gameMove: function (rownumber,colnumber,letter,gameid,id,success) {
  	rownumber=parseInt(rownumber);
  	colnumber=parseInt(colnumber);

    	if(isNaN(rownumber) || isNaN(colnumber) || (!letter.match(/[a-z]/i)) || letter.length>1){
    		success({err:true,msg:"There is Some Problem!!"});
    	}
    	else if(isValidObjectID(gameid)){
    		var gameid = new ObjectId(gameid);
       		Games.find({$and:[ {_id:gameid}, {$or:[{user1:id},{user2:id}]} ] },function(err,data){
            if(err) success({err:true,msg:err});
            else if(!data[0]) success({err:true,msg:"Your Don't have Access to this Game"});
            else{
           			var length=data[0].Moves.length;
           			if(length==1){
           				if(data[0].user1==id){
           					Games.findOneAndUpdate({_id:gameid}, {$addToSet :{ Moves:{$each:[{row:rownumber,col:colnumber,letter:letter,actionBy:id}]}} } ,{safe: true, upsert: true},function(err, updateData1) {
           		       if(err) success({err:true,msg:err})
                     else success({err:false,msg:{row:rownumber,col:colnumber,letter:letter,actionBy:id}});
           					});
           				}else{
           					success({err:true,msg:"Its Not your Turn"});
           				}
           			}else if(length==2){
           				if(data[0].user2==id){
           					Games.findOneAndUpdate({_id:gameid}, {$addToSet :{ Moves:{$each:[{row:rownumber,col:colnumber,letter:letter,actionBy:id}]}} } ,{safe: true, upsert: true},function(err, updateData1) {
           					   if(err) success({err:true,msg:err})
                       else success({err:false,msg:{row:rownumber,col:colnumber,letter:letter,actionBy:id}});
           					});
           				}else{
           					success({err:true,msg:"Its Not your Turn"});
           				}
           			}else{
        	   			if(data[0].Moves[length-1].actionBy!=id && data[0].Moves[length-2]!=id){
        		   			Games.findOneAndUpdate({_id:gameid}, {$addToSet :{ Moves:{$each:[{row:rownumber,col:colnumber,letter:letter,actionBy:id}]}} } ,{safe: true, upsert: true},function(err, updateData1) {
        		   			     if(err) success({err:true,msg:err})
                         else success({err:false,msg:{row:rownumber,col:colnumber,letter:letter,actionBy:id}});
        		   			});
        	   			}else{
        	   				success({err:true,msg:"Its Not your Turn"});
        	   			}
        	   		}
               }
          });
  	}else{
  	   		success({err:true,msg:"Invalid Id"});
  	}
  },

  chat:function(gameid,id,msg,success){
    if( isValidObjectID(gameid) && isValidObjectID(id) ){
      var gameid = new ObjectId(gameid);
      Games.find({$and:[ {_id:gameid}, {$or:[{user1:id},{user2:id}]} ] },function(err,data){
        if(err) success({err:true,msg:err});
        else if(!data[0]) success({err:true,msg:"Your Don't have Access to this Game"});
        else{
             Chat.collection.insert({gameId:gameid,from:id,message:msg},function(err,data){
              if(err) success({err:true,msg:err})
              else success({err:false,msg:msg});
            });
        }
      });
    }else{
          success({err:true,msg:"Invalid Id"});
    }  
  },
  checkGameEnd:function(gameid,id,success){
    if( isValidObjectID(gameid) && isValidObjectID(id) ){
      var gameid = new ObjectId(gameid);
        if(data[0].Moves.length==totallength-1){
              Games.find({_id:gameid},function(err,data){
                  if(err) success({err:true,msg:err});
                  else{
                       var gridSize=data[0].gridSize;
                       var totallength=gridSize*2;
                       var result=data[0].Moves.filter(function (el) {
                          return el.actionBy ==data[0].user1 ||  el.actionBy == 'Bot';
                        });

                       var result2=data[0].Moves.filter(function (el) {
                          return el.actionBy ==data[0].user2 ||  el.actionBy == 'Bot';
                        });

                       var user1Moves=[],user2Moves=[];

                       for(var i=0;i<gridSize;i++){
                            user1Moves[i]=[];
                            user2Moves[i]=[];
                          for(var j=0;j<gridSize;j++){
                             var t1=result.filter(function(e){ return e.row==i && e.col==j; });
                             if(t1.length>0){
                                user1Moves[i][j]=t1[0].letter;
                              }else{
                                user1Moves[i][j]='';
                              }
                             var t2=result2.filter(function(e){ return e.row==i && e.col==j; });
                             if(t2.length>0){
                                user2Moves[i][j]=t2[0].letter;
                              }else{
                                user2Moves[i][j]='';
                              }
                          }
                       }

                       var score1=calculateScore(gridSize,user1Moves);
                       var score2=calculateScore(gridSize,user2Moves);

                       Games.collection.update({_id:gameid},{$set:{score1:score1,score2:score2}},function(err,data){
                        if(err) success({err:true,msg:err});
                        else{
                            Games.find({_id:gameid},function(err,data){
                              success({success:true,info:data});
                            });
                        }
                       });
                  }
              });

        }else{
          success({success:false,info:"Game Not Ended Yet!!"});
        }

    }
  }

};