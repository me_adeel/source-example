var dic = require('../routes/dictionary');
var dictionary = dic.dictionary();
var Place=[];
var HintSearch = [];
var AlreadyScored = [];
var FinalWord = [];

function SearchAllDictionary() {
    var bool1 = 0;
    var sindex = 0;
    var eindex = 0;

    HintSearch.sort(function(a, b) {
        if (a[1] < b[1]) return 1;
        else return -1;
    });

    HintSearch.sort(function(a, b) {
        if (a[0].length < b[0].length) return 1;
        else return -1;
    });

    HintSearch.sort(function(a, b) {
        if (a[1] < b[1]) return 1;
        else return -1;
    });

    for (var i = 0; i < HintSearch.length; i++) {
        
        if (HintSearch[i][0][0] == '_'){
            bool1 = 1;
        }else{
            bool1 = 0;
        }

        if (bool1 == 1) {
            for (var n = 0; n < 26; n++) {
                var arr2 = [];
                for (var l = 0; l < HintSearch[i][0].length; l++) {
                    if (HintSearch[i][0][l] != '_')
                        arr2.push(l);
                }

                var val = String.fromCharCode(65 + n) + HintSearch[i][0].length;
                var c = searchDictionaryForHint(val, HintSearch[i][0], arr2);
                if (c != false) {
                    FinalWord = [];
                    FinalWord.push(HintSearch[i], c);
                    return FinalWord;
                }
            }
        } else {
            var arr2 = [];
            for (var l = 0; l < HintSearch[i][0].length; l++) {
                if (HintSearch[i][0][l] != '_')
                    arr2.push(l);
            }

            var val = HintSearch[i][0][0] + HintSearch[i][0].length;
            var c = searchDictionaryForHint(val, HintSearch[i][0], arr2);
            if (c != false) {
                FinalWord = [];
                FinalWord.push(HintSearch[i], c);
                return FinalWord;
            }

        }
    }
    return false;
}

function searchDictionaryForHint(val, searchVal, arr2search) {
	val=val.toUpperCase();
    for (var i = 0; i < dictionary[val].length; i++) {
        for (var j = 0; j < arr2search.length; j++) {
            if (dictionary[val][i][arr2search[j]].toUpperCase() != searchVal[arr2search[j]]) {
                break;
            }
        }
        if (j == arr2search.length && AlreadyScored.indexOf(dictionary[val][i].toUpperCase()) == -1){
            return dictionary[val][i];
        }
    }
    return false;
}


module.exports = {
      getHint:function(gridLength,array) {
    	Place=array;

        var letterscount = 0;
        for (var i = 0; i < gridLength; i++) {
            for (var j = 0; j < gridLength - 2; j++) {
                word = '';
                letterscount = 0;
                for (var k = 0; k < gridLength - j; k++) {
                    var id = Place[i][j+k];
                    if (id!='_') {
                        letterscount++;
                        word += id;
                    } else {
                        word += '_';
                    }
                    if (word.length > gridLength-2) {
                        if (AlreadyScored.indexOf(word) == -1 && HintSearch.indexOf(word) == -1 && letterscount < word.length) {
                            HintSearch.push([word, letterscount, "r_"+i+"_"+(j + k - word.length + 1) + "_" + (j + k)]);
                        }
                    }
                }
            }
        }
        for (var i = 0; i < gridLength; i++) {
            for (var j = 0; j < gridLength - 2; j++) {
                word = '';
                letterscount = 0;
                for (var k = 0; k < gridLength - j; k++) {
                    var id = Place[j+k][i];
                    if (id!='_') {
                        word += id;
                        letterscount++;
                    } else {
                        word += '_';
                    }
                    if (word.length > gridLength-2) {
                        if (AlreadyScored.indexOf(word) == -1 && HintSearch.indexOf(word) == -1 && letterscount < word.length) {
                            HintSearch.push([word, letterscount, "c_"+i+"_"+(j+k-word.length+1)+ "_" + (j + k)]);
                        }
                    }

                }
            }
        }

        var finals = SearchAllDictionary();
        FinalWord = [];
        HintSearch = [];
        return finals;
    }
};