var express = require('express')
var router = express.Router();
var mongoose = require('mongoose')
var Users=mongoose.model('Users');
var Friends=mongoose.model('Friends');
var Games=mongoose.model('Games');
var LeaderBoard=mongoose.model('LeaderBoard');
var path = require('path'),fs = require('fs');
var ObjectId = require('mongoose').Types.ObjectId; 
var passwordHash = require('password-hash');
var nodemailer = require('nodemailer');


function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function usernameExists(req,username,success){
	Users.find({username:username},function(err,data){
		if(err)console.log(err);
		if(data.length>0){
			success(true);
		}else{
			success(false);
		}
	});
}

function emailExists(req,email,success){
	Users.find({email:email},function(err,data){
		if(data.length>0){
			success(true);
		}else{
			success(false);
		}
	});
}

function getDefaultError() {
	return {
		errmsg: "Some problem occured, please try again later"
	};
};

//Register
router.post("/register",function(req,res){
	var username=req.body.username;
	var name=req.body.name;
	var password=req.body.password;
	var cpassword=req.body.cpassword;
	var email=req.body.email;
	var err=getDefaultError(),data={};
	res.status(400);
	console.log(req.body);
	console.log(req.body.username);
	console.log(username,name,password,cpassword,email);
	usernameExists(req,username,function(data1){
		if(!data1){
			emailExists(req,email,function(data2){
				console.log(data2)
				if(!data2){
					if(name!=null && name.length<2){
						err.errmsg="Please enter a valid Name, Must be greater than 2 characters";
						res.send({error:err});
					}else if(username!=null && username.length<8){
						err.errmsg="Username Must be minimum 8 characters";
						res.send({error:err});
					}else if(password!=null && password.length<8){
						err.errmsg="Password Must be minimum 8 characters";
						res.send({error:err});
					}else if(password!=cpassword){
						err.errmsg="Password and Confirm Password must be Same";
						res.send({error:err});
					}else if(email!=null && !validateEmail(email)){
						err.errmsg="Please Enter correct Email Address";
						res.send({error:err});
					}else if(name!=null && username!=null && password!=null && email!=null){
						var hashedPassword = passwordHash.generate(password);
						Users.collection.insert({name:name,username:username,password:hashedPassword,email:email,verified:false,picId:null},function(err3,data3){
							if(err3){
								res.send({error:err});
							}else{
								data.msg="Thanks for registration, Please Check Email";
								res.status(200);
								res.send({error:null,data:data});
							}
						});
					}else{
						err.errmsg="Please Fill All Required Fields";
						res.send({error:err});
					}
				}else{
					err.errmsg="Email Already Exists";
					res.send({error:err});
				}
			});
		}else{
			err.errmsg="Username already Exists";
			res.send({error:err});
		}
	});
});

//Login
router.post("/login",function(req,res){
	var email=req.body.email;
	var password=req.body.password;
	var err=getDefaultError(),data={};
	
	res.status(400);

	if(email!=null && !validateEmail(email)){
		err.errmsg="Please Enter correct Email Address";
		res.send({error:err,data:data});
	}else if(password!=null && password.length<8){
		err.errmsg="Password Must be minimum 8 characters";
		res.send({error:err,data:data});
	}else if(email!=null && password!=null){
		Users.find({email:email},function(err1,data1){
			if(err1){
			 err.errmsg="Some Problem occured, Please try again later";
			 res.send({error:err,data:data});
			}else if(data1.length>0 && passwordHash.verify(password, data1[0].password)){
				req.session.id=data1[0]._id;
				req.session.email=data1[0].email;
				res.status(200);
				res.send({error:null,data:data1[0]});
			}else{
				err.errmsg="Incorrect email and/or password";
				res.send({error:err,data:data});
			}
		});
	}else{
		err.errmsg="Please Fill All Required Fields";
		res.send({error:err,data:data});
	}
	
});

router.post("/flogin",function(req,res){
	var fbToken=req.body.fbToken,err=getDefaultError();
	var email,name,id;
	res.status(400);

	if(fbToken==null){
		err.errmsg="Fbtoken Can't be Null";
		res.send({error:err,data:data});
	}
	else{
		var request = require('request');
		request.get("https://graph.facebook.com/me?fields=id,name,email&access_token="+fbToken, {json: true}, function(err1, response, body) {
	      if (!err1 && response.statusCode === 200) {
	          email=body.email,name=body.name,id=body.id
	         	if(email==null){
					err.errmsg="Problem in Fetching Email Address";
					res.send({error:err,data:data});
				}else if(name==null){
						err.errmsg="Problem in Fetching Name";
						res.send({error:err,data:data});
				}else if(id==null){
						err.errmsg="Problem in Fetching Id";
						res.send({error:err,data:data});
				}else{
					Users.find({email:email},function(err2,data2){
						if(err2){
						 	err.errmsg="Some Problem occured, Please try again later";
						 	res.send({error:err,data:data});
						}else if(data2.length>0){
							req.session.id=data2[0]._id;
							req.session.email=data2[0].email;
							res.status(200);
							res.send({error:null,data:data2[0]});
						}else{
							Users.collection.insert({name:name,email:email,verified:true,picId:null,fbId:id},function(err3,data3){
								if(err3){
								 	err.errmsg="Some Problem occured, Please try again later";
								 	res.send({error:err,data:data});
								}else{
									console.log(data3);
									req.session.id=data3.ops[0]._id;
									req.session.email=data3.ops[0].email;
									res.status(200);
									res.send({error:null,data:data3.ops[0]});
								}
							});
						}
					});

				}

	      }else{
	      	err.errmsg="Invalid token";
	      	res.send({error:err,data:data});
	      }
	  	});
	}
});


router.post("/forgetPassword",function(req,res){
	var email=req.body.email,err=getDefaultError();
	if(email==null){
		errmsg.data="Email Can't be Null";
		res.send({error:err,data:data});
	}else{

		var transporter = nodemailer.createTransport({
		    service: 'Gmail',
		    auth: {
		        user: 'kelashbatra@gmail.com',
		        pass: 'izunhcbwmsocckjc'
	    	}
		});
		Users.find({email:email},function(err,data){
			if(err)res.send({error:true,data:err});
			else if(data.length>0){
				var mailOptions = {
				    from: 'Test L2f App <info@letter2friends.com>', // sender address
				    to: email, // list of receivers
				    subject: 'Forget Password', // Subject line
				    text: 'Click on Link to Reset Password', // plaintext body
				    html: '<a href="http://localhost:3001/resetPassword/'+data[0].password+'">Click Here!!</a>' // html body
				};
				transporter.sendMail(mailOptions, function(error, info){
				    if(error){
				        res.send({error:true,data:error});
				    }else{
				    	res.send({error:false,data:"Mail Sent"});
				    }
				});
			}else{
				res.send({error:true,data:"No Such Email Exists"});
			}
		});

	}
});


router.post("/resetPassword",function(req,res){
	var key=req.body.key,password=req.body.password,err=true;
	if(key==null){
		data="Key Can't be Null";
		res.send({error:err,data:data});
	}else if(password==null || password.length<8){
		data="Password Must be minimum 8 characters";
		res.send({error:err,data:data});
	}else{
		Users.find({password:key},function(err,data){
			if(err)res.send({error:true,data:err});
			else if(data.length>0){
				var hashedPassword = passwordHash.generate(password);
				Users.collection.update({password:key},{$set:{password:hashedPassword}},function(err,data){
					if(err){
						res.send({error:true,data:err});
					}else{
						data="Password is Updated";
						res.send({error:false,data:data});
					}
				});
			}else{
				res.send({error:true,data:"Invalid Key"});
			}
		});
	}
});


module.exports = router;