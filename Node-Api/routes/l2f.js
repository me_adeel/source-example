var express = require('express')
var router = express.Router();
var mongoose = require('mongoose')
var Users=mongoose.model('Users');
var Friends=mongoose.model('Friends');
var Payment=mongoose.model('Payment');
var Games=mongoose.model('Games');
var Chat=mongoose.model('Chat'); 
var LeaderBoard=mongoose.model('LeaderBoard');
var  path = require('path'),fs = require('fs');
var ObjectId = require('mongoose').Types.ObjectId; 
var passwordHash = require('password-hash');
var Alphabet="QWERTYUIOPASDFGHJKLZXCVBNM";

function myMiddleware (req, res, next) {
  	res.status(401);
	var response = {
  		error: {errmsg: "Unauthorized"},
  	};
	if(req.session.id!==undefined && req.session.email!==undefined){
		Users.find({_id:req.session.id},function(err,data){
			if (err || data.length == 0) {
				res.send(response);
			}
			else {
				res.status(200);
				next();
			}
		});
	  }else{
	  	res.send(response);
	 }
}

function isValidObjectID(str) {
  // coerce to string so the function can be generically used to test both strings and native objectIds created by the driver
  str = str + '';
  var len = str.length, valid = false;
  if (len == 12 || len == 24) {
    valid = /^[0-9a-fA-F]+$/.test(str);
  }
  return valid;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function updateUsernameExists(req,username,success){
	var id = new ObjectId(req.session.id);
	Users.find({username:username,_id:{$ne:id}},function(err,data){
		if(err)console.log(err);
		if(data.length>0){
			success(true);
		}else{
			success(false);
		}
	});
}

function updateEmailExists(req,email,success){
	var id = new ObjectId(req.session.id);
	Users.find({email:email,_id:{$ne:id}},function(err,data){
		if(data.length>0){
			success(true);
		}else{
			success(false);
		}
	});
}

function verifyFriendId(fId,success){
	var id = new ObjectId(fId);
	Users.find({_id:id},function(err,data){
		if(data.length>0){
			success(true);
		}else{
			success(false);
		}
	});
}


function verifyFriendRequest(req,fId,success){
	Friends.find({$or:[{userId:req.session.id,friendId:fId},{userId:fId,friendId:req.session.id}]},function(err,data){
		if(data.length>0){
			success(true);
		}else{
			success(false);
		}
	});
}

function verifyFriendAction(req,requestId,success){
	var id = new ObjectId(requestId);
	Friends.find({_id:id,friendId:req.session.id},function(err,data){
		if(data.length>0){
			success(true);
		}else{
			success(false);
		}
	});
}


function verifyGameAction(req,requestId,success){
	var id = new ObjectId(requestId);
	Games.find({_id:id,user2:req.session.id},function(err,data){
		if(data.length>0){
			success(true);
		}else{
			success(false);
		}
	});
}

function canSeeMoves(req,gameId,success){
	var id = new ObjectId(gameId);
	Games.find({ $and:[ {_id:id},{$or:[{user1:req.session.id},{user2:req.session.id}]} ] },function(err,data){
		if(data.length>0){
			success(true);
		}else{
			success(false);
		}
	});
}

function getrandomdId()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 16; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function getDefaultError() {
	return {
		errmsg: "Some problem occured, please try again later"
	};
};

//MiddleWare
router.use("/",myMiddleware);

//Get Profile
router.get("/profile",function(req,res){
	var id = new ObjectId(req.session.id);
	var err=getDefaultError(),data={};
	Users.find({_id:id},function(err,data1){
		if(err){
			 err.errmsg="Some Problem occured, Please try again later";
			 res.send({error:err,data:data});
		}else{
			console.log(data1);
			res.send({error:null,data:data1[0]});
		}
	});
});

//Update Profile
router.post("/updateProfile",function(req,res){
	var id = new ObjectId(req.session.id);
	var username=req.body.username;
	var name=req.body.name;
	var password=req.body.password;
	var cpassword=req.body.cpassword;
	var email=req.body.email;
	var err=true,data="";

	updateUsernameExists(req,username,function(data){
		if(!data){
			updateUsernameExists(req,email,function(data){
				if(!data){
					if(name!=null && name.length<4){
						data="Please enter a valid Name";
						res.send({error:err,data:data});
					}else if(username!=null && username.length<9){
						data="Username Must be 8 characters";
						res.send({error:err,data:data});
					}else if(password!=null && password.length<9){
						data="Password Must be 8 characters";
						res.send({error:err,data:data});
					}else if(password!=cpassword){
						data="Password and Confirm Password must be Same";
						res.send({error:err,data:data});
					}else if(email!=null && !validateEmail(email)){
						data="Please Enter correct Email Address";
						res.send({error:err,data:data});
					}else if(name!=null && username!=null && password!=null && email!=null){
						err=false;
						var hashedPassword = passwordHash.generate(password);
						Users.collection.update({_id:id},{name:name,username:username,password:hashedPassword,email:email,verified:false,picId:null},function(err,data){
							if(err){
								data=err;
								res.send({error:err,data:data});
							}else{
								data="Profile SuccessFully Updated";
								res.send({error:err,data:data});
							}
						});
					}else if(name!=null && username!=null && password==null && email!=null){
						err=false;
						Users.collection.update({_id:id},{$set:{name:name,username:username,email:email,verified:false,picId:null}},function(err,data){
							if(err){
								data=err;
								res.send({error:err,data:data});
							}else{
								data="Profile SuccessFully Updated";
								res.send({error:err,data:data});
							}
						});
					}else{
						data="Please Fill Add Required Fields";
						res.send({error:err,data:data});
					}
				}else{
					data="Email Already Exists";
					res.send({error:err,data:data});
				}
			});
		}else{
			data="Username already Exists";
			res.send({error:err,data:data});
		}
	});
});



//Add Friends
router.post("/friends",function(req,res){
	var fId=req.body.fId,id=req.session.id;
	if(fId==null){
		res.send({error:true,data:"Please Select Friend"});
	}else if(!isValidObjectID(fId)){
		res.send({error:true,data:"Invalid Friend Id"});
	}else if(fId!=id){
		verifyFriendRequest(req,fId,function(data){
			if(!data){
				verifyFriendId(fId,function(data){
					if(data){
						Friends.collection.insert({userId:id,friendId:fId,status:0},function(err,response){
							if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
							else res.send({error:false,data:"Request Sent!!"});
						});
					}else{
						res.send({error:true,data:"Invalid Friend Id"});
					}
				});
			}else{
				res.send({error:true,data:"Request is already Sent!"});		
			}
		});
	}else{
			res.send({error:true,data:"You Can't send Request to Yourself"});
	}
});



function FriendsGetName(response,success){
	if(response.length==0){
		success([]);
	}
	 var all=[];
	response.forEach(function(e){
		e=JSON.parse((JSON.stringify(e)));
		Users.find({_id:e.userId},function(err,d){
			console.log(d);
	     	e.name=d[0].name;
	     	all.push(e);
	     	if(all.length==response.length){
	     		success(all);	
	     	}
	     });
    });
}

//Get Friends List With Custom Status
router.get("/friends/:status/:page*?",function(req,res){
	var limit=1,skip=0,id=req.session.id,status=0;

	if(req.params.status!=null){
      status=parseInt(req.params.status);
	} 
	
	if(req.params.page!=null){
      skip=parseInt(req.params.page)*limit;
	}  
	Friends.find({friendId:id,status:status},{},{skip:skip,limit:limit},function(err,response){
		if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
		else{
			FriendsGetName(response,function(data){
				res.send({error:false,data:data});
			});	
		}
	});
});



//Approve or Reject Request
router.post("/friendsAction",function(req,res){
	var requestId=req.body.requestId,id=req.session.id,action=req.body.action;

	if(requestId==null){
		res.send({error:true,data:"Please Select Request Id"});
	}else if(action==null){
		res.send({error:true,data:"Please Select Action"});
	}else if(action==1 || action==2){
		verifyFriendAction(req,requestId,function(data){
			if(data){
				Friends.collection.update({_id:new ObjectId(requestId)},{ $set:{status:action} },function(err,response){
					if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
					else if(action==1){
						res.send({error:false,data:"Request Approved!!"});
					}else{
						res.send({error:false,data:"Request Rejected!!"});
					}
				});
			}else{
				res.send({error:true,data:"You don't have access to Approve/reject this Request"});
			}
		});
	}else{
		res.send({error:true,data:"Invalid Action"});
	}
});



//Add Game
router.post("/games",function(req,res){
	var gridSize=req.body.gridSize,fId=req.body.fId,id=req.session.id;

	if(gridSize==null || gridSize<5 || gridSize>7){
		res.send({error:true,data:"Please Enter correct GridSize from 5-7"});
	}else if(fId==null){
		res.send({error:true,data:"Please Select Friend"});
	}else if(!isValidObjectID(fId)){
		res.send({error:true,data:"Invalid Friend Id"});
	}else if(fId!=id){
		verifyFriendId(fId,function(data){
			if(data){
				var row=getRandomInt(1,gridSize);
				var col=getRandomInt(1,gridSize);
				var l=getRandomInt(1,26);
				var letter=Alphabet[l];

				Games.collection.insert({gridSize:gridSize,user1:id,user2:fId,score1:0,score2:0,status:0,Moves:[{row:row,col:col,letter:letter,actionBy:'Bot'}]},function(err,response){
					if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
					else res.send({error:false,data:"Request Sent!!"});
				});

			}else{
				res.send({error:true,data:"Invalid Friend Id"});
			}
		});
	}else{
		res.send({error:true,data:"You Can't send Request to Yourself"});
	}

});



function GameGetName(response,success){
	if(response.length==0){
		success([]);
	}
	 var all=[];
	response.forEach(function(e){
		e=JSON.parse((JSON.stringify(e)));
		Users.find({_id:e.user1},function(err,d){
			console.log(d);
	     	e.name=d[0].name;
	     	all.push(e);
	     	if(all.length==response.length){
	     		success(all);	
	     	}
	     });
    });
}


//Get Game List With Custom Status
router.get("/games/:status/:page*?",function(req,res){
	var limit=1,skip=0,id=req.session.id,status=0;

	if(req.params.status!=null){
      status=parseInt(req.params.status);
	} 
	
	if(req.params.page!=null){
      skip=parseInt(req.params.page)*limit;
	}  

	Games.find({user2:id,status:status},{},{skip:skip,limit:limit},function(err,response){
		if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
		else{
			GameGetName(response,function(data){
				res.send({error:false,data:data});
			});	
		}
	});

});




//Approve or Reject Game Request
router.post("/gamesAction",function(req,res){
	var requestId=req.body.requestId,id=req.session.id,action=req.body.action;

	if(requestId==null){
		res.send({error:true,data:"Please Select Request Id"});
	}else if(action==null){
		res.send({error:true,data:"Please Select Action"});
	}else if(action==1 || action==2){
		verifyGameAction(req,requestId,function(data){
			if(data){
				Games.collection.update({_id:new ObjectId(requestId)},{ $set:{status:action} },function(err,response){
					if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
					else if(action==1){
						res.send({error:false,data:"Request Approved!!"});
					}else{
						res.send({error:false,data:"Request Rejected!!"});
					}
				});
			}else{
				res.send({error:true,data:"You don't have access to Approve/reject this Request"});
			}
		});
	}else{
		res.send({error:true,data:"Invalid Action"});
	}
});



//Create a new Random Game
router.post("/randomGame",function(req,res){
	var gridSize=req.body.gridSize,id=req.session.id;
	if(gridSize==null || gridSize<5 || gridSize>7){
		res.send({error:true,data:"Please Enter correct GridSize from 5-7"});
	}else{
		Games.find({$and:[{user2:null},{user1:{$ne:id}}]},{},{limit:1},function(err,response){
			if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
			else if(response.length>0){
				Games.collection.update({_id:response[0]._id},{ $set:{user2:id,status:1} },function(err,response){
					if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
					else res.send({error:false,data:{gameid:response.ops[0]._id,data:"You have Joined Random Game"}});
				});
			}else{
				var row=getRandomInt(1,gridSize);
				var col=getRandomInt(1,gridSize);
				var l=getRandomInt(1,26);
				var letter=Alphabet[l];

				Games.collection.insert({gridSize:gridSize,user1:id,user2:null,score1:0,score2:0,status:0,Moves:[{row:row,col:col,letter:letter,actionBy:'Bot'}]},function(err,response){
					if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
					else res.send({error:false,data:{gameid:response.ops[0]._id,data:"New Game Created.. Please Wait For New User !!"}});
				});
			}
		});
	}
});


//Leaderboard Score
router.get("/leaderboard",function(req,res){
	LeaderBoard.find({gridSize:5},{},{limit:10, sort:{ score: -1 }},function(err,response1){
		if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
		LeaderBoard.find({gridSize:6},{},{limit:10, sort:{ score: -1 }},function(err,response2){
			if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
			LeaderBoard.find({gridSize:7},{},{limit:10, sort:{ score: -1 }},function(err,response3){
				if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
				else res.send({error:false,data:response1.concat(response2).concat(response3)});
			});
		});
	});
});


//Get game all moves
router.get("/allMoves/:gameId",function(req,res){
	var gameId=req.params.gameId;
	canSeeMoves(req,gameId,function(data){
		if(data){
			Games.find({_id:gameId},function(err,response){
				if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
				else res.send({error:false,data:response[0].Moves});
			});
		}else{
				 res.send({error:true,data:"You can See All Moves of this Game"});
		}
	});
});

//Payment Records
router.post("/payment",function(req,res){
	var platform=req.body.platform,transaction=req.body.transaction,payment=req.body.payment,packageType=req.body.packageType;
	var id=req.session.id;
	if(platform==null){
			res.send({error:true,data:"Please Insert Platform type"});
	}else if(transaction==null){
			res.send({error:true,data:"Please Insert transaction type"});
	}else if(payment==null){
			res.send({error:true,data:"Payment can't be Null "});
	}else if(packageType==null){
			res.send({error:true,data:"packageType can't be Null"});
	}else{
		Payment.collection.insert({platform:platform,transaction:transaction,payment:payment,packageType:packageType},function(err,response){
			if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
			else{
				var coins=100;
				Users.collection.update({_id:new ObjectId(id)},{$inc:{coins:coins}},function(err,response){
					if(err) res.send({error:false,data:err});
					else res.send({error:false,data:"SuccessFully Updated Coins"});
				});
			}
		});
	}
});

//Hint/Mover -- Coins Deduct

function HintMover(id,success){
	Users.find({_id:new ObjectId(id)},function(err,data){
		if(err) success(false);
		else{
			if(data[0].coins>=10){
				Users.collection.update({_id:new ObjectId(id)},{$inc:{coins:-10}},function(err,response){
					if(err) success(false);
					else success(true);					
				});
			}else{
				success(false);
			}
		}
	});
}

//Mover

router.post("/mover",function(req,res){
	var gameid=req.body.gameid,row=req.body.fromrow,col=req.body.fromcol,row2=req.body.torow,col2=req.body.tocol;
	var id=req.session.id;

	if(gameid==null){
		res.send({error:true,data:"Game Id Can't be null "});
	}else if(row==null){
		res.send({error:true,data:"From Row Can't be null "});
	}else if(col==null){
		res.send({error:true,data:"From Col Can't be null "});
	}else if(row2==null){
		res.send({error:true,data:"To Row Can't be null "});
	}else if(col2==null){
		res.send({error:true,data:"To Col Can't be null "});
	}else{
	    var rownumber=parseInt(row);
	    var colnumber=parseInt(col);
	    var rownumber2=parseInt(row2);
	    var colnumber2=parseInt(col2);

	    HintMover(req.session.id,function(data){
	      if(data){
	         if(isValidObjectID(gameid)){
	          	canSeeMoves(req,gameid,function(data){
	          	if(data){
	          		gameid = new ObjectId(gameid);
		          	 Games.find({_id:gameid},{Moves:{ $elemMatch:{row:rownumber,col:colnumber,actionBy:id}}},function(err,data){
		          	 	console.log(data);
		          	  if(data[0].Moves.length>0){
			              Games.collection.update({_id:gameid,"Moves.row":rownumber,"Moves.col":colnumber},{$set:{"Moves.$.row":rownumber2,"Moves.$.col":colnumber2}},function(err,data2){
				                if(err) res.send({err:true,data:err})
					            else{
				                  	res.send({err:false,data:"success"});
				              	}
			                });
			              }else{
			              	res.send({error:true,data:"Now such move position is in Db"});
			              }
			          });
		           }else{
		            	res.send({error:true,data:"You Don't have access to game"});
		           }
		        });
	          }else{
	          		res.send({error:true,data:"Invalid Game Id"});
	          }
	      }else{
	          res.send({err:true,data:"You don't have enought Coins"});
	      }
	    });
	}
});


function checkStatus(gridSize,arr){
	for(var i=0;i<gridSize;i++){
		for(var j=0;j<gridSize;j++){
			if(arr[i][j]=== undefined){
				return true;
			}
		}
	}
	return false;
}

//Hint
router.post("/hint",function(req,res){
	var gridSize=req.body.gridSize,arr=req.body.arr;
	var forward=false;

	if (arr!=null){
	    try{
	        arr = arr.replace(/'/g, '"');
			arr=JSON.parse(arr);
			forward=true;
	    }catch(e){
	    	forward=false;
	        res.send({err:true,data:"There is some problem in Your Array"});
	    }
	}
	
	if(forward){
		if(gridSize==null || gridSize<5 || gridSize>7){
			res.send({error:true,data:"GridSize Can't be null and Must be 5-7 "});
		}else if(arr[1] == undefined || arr[1][2] == undefined || checkStatus(gridSize,arr)){
			res.send({error:true,data:"Invalid 2d Array "});
		}else{
			 HintMover(req.session.id,function(data){
		      if(data){
		      		var Hint = require('../routes/hint');
		      		var temp = Hint.getHint(gridSize,arr);
		      		if(temp){
		      			res.send({ err:false, data:{line:temp[0][0],size:temp[0][1],location:temp[0][2],word:temp[1]}});
		      		}else{
		      			Users.collection.update({_id:new ObjectId(id)},{$inc:{coins:10}},function(err,response){
							if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
							else res.send({err:true,data:"Couldn't find Any hint"});				
						});				
		      		}
		      }else{
		      	res.send({err:true,data:"You don't have enought Coins"});
		      }
		  });
		}
	}

});


//Search Users
router.post("/search",function(req,res){
	var name=req.body.name;
	if(name==null){
		res.send({error:true,data:"Name Can't be null "});
	}else if(name.length>3){
		Users.find({'name': {'$regex': name}},{name:1,_id:1,email:1},function(err,response){
			if(err) res.send({error:true,data:"Some Problem occured, Please try again later"});
			else res.send({error:false,data:response});
		});
	}else{
		res.send({error:true,data:"Please Enter at Least 3 Characters"});
	}
});


//Chat
router.post("/chat",function(req,res){
	var gameid=req.body.gameid,id=req.session.id;
	if(gameid==null){
			res.send({error:true,data:"Game Id Can't be null "});
	}else if( isValidObjectID(gameid) && isValidObjectID(id) ){
		gameid = new ObjectId(gameid);
		canSeeMoves(req,gameid,function(data){
			if(data){
				Chat.find({gameId:gameid},function(err,data){
			      if(err) res.send({err:true,data:err})
			      else res.send({err:false,data:data});
			    });	
			}else{
				 res.send({err:true,data:"You Don't have access to This"});
			}
		});
    }else{
          res.send({err:true,data:"Invalid Id"});
    }
});

module.exports = router;