// Setup basic express server
var async = require('async');
var express = require('express');
var path = require('path');
var io;
var port = 3001;
var userdata = {};
var sockets = {};
var games = {};
var numUsers = 0;

var net = require('net'),
    cluster = require('cluster');

var env = process.env.NODE_ENV || 'development';
var envConfig = require('./config/env')[env];

function hash(ip, seed) {
    var hash = ip.reduce(function(r, num) {
        r += parseInt(num, 10);
        r %= 2147483648;
        r += (r << 10)
        r %= 2147483648;
        r ^= r >> 6;
        return r;
    }, seed);

    hash += hash << 3;
    hash %= 2147483648;
    hash ^= hash >> 11;
    hash += hash << 15;
    hash %= 2147483648;

    return hash >>> 0;
}


var server;

// `num` argument is optional
if (typeof num !== 'number')
num = require('os').cpus().length - 1;

// Master will spawn `num` workers
if (cluster.isMaster) {
    var workers = [];
    for (var i = 0; i < num; i++) {
        ! function spawn(i) {
            workers[i] = cluster.fork();
            // Restart worker on exit
            workers[i].on('exit', function() {
                console.error('sticky-session: worker died');
                spawn(i);
            });
        }(i);
    }

    var seed = ~~(Math.random() * 1e9);
    server = net.createServer(function(c) {
        // Get int31 hash of ip
        var worker,
            ipHash = hash((c.remoteAddress || '').split(/\./g), seed);
        // console.log(c.remoteAddress);
        // Pass connection to worker
        worker = workers[ipHash % workers.length];
        worker.send('sticky-session:connection', c);
    }).listen(port);
} else {
    startServer();
}



function doStickyClusterStuff() {
    // Worker process
    process.on('message', function(msg, socket) {
        if (msg !== 'sticky-session:connection') return;

        server.emit('connection', socket);
    });

    if (!server) throw new Error('Worker hasn\'t created server!');

    // Monkey patch server to do not bind to port
    var oldListen = server.listen;
    server.listen = function listen() {
        var lastArg = arguments[arguments.length - 1];

        if (typeof lastArg === 'function') lastArg();

        return oldListen.call(this, null);
    };
}


function startServer() {
    var app = new express();
    var cors = function (req, res, next) {
        
        res.header("Access-Control-Allow-Origin", 'http://192.168.0.102:3000');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header("Access-Control-Allow-Credentials", "true");
        next();
    };
    app.use(cors);

    require('./config/config')(app, envConfig);
    require('./config/database')(envConfig)
    require('./config/routes')(app);
    var functions=require('./routes/functions');
    var cookieSession = require('cookie-session');
    var cookieParser = require('cookie-parser')();
     var session=cookieSession({
      name: "session",
      secret: "l2fApp",
      maxAge: 15724800000
    });
    
    app.use(session);
 
    app.use("/public", express.static(path.join(__dirname, 'public')));

    // Don't expose our internal server to the outside.
    server = app.listen(0, 'localhost'),
    io = require('socket.io')(server);
     io.use(function(socket, next) {
        var req = socket.handshake;
        var res = {};
        cookieParser(req, res, function(err) {
            if (err) return next(err);
            session(req, res, next);
        });
    });

    doStickyClusterStuff();

    console.log("listening to port " + port);
    io.adapter(require('socket.io-redis')({
        host: 'localhost',
        port: 6379
    }));

    io.on('connection', function(socket) {


        console.log("new connection");
    
        userdata[socket.id]= socket.handshake.session.id;
        sockets[socket.id] = socket;


        socket.on('game',function(data){
            socket.join(data);
            games[socket.id]=data;
        });

        socket.on('move',function(row,col,letter){
            console.log(userdata[socket.id]);
            if(games[socket.id]!=null){
                functions.gameMove(row,col,letter,games[socket.id],userdata[socket.id],function(data){
                    if(!data.err){
                        socket.broadcast.to(games[socket.id]).emit("move",data.msg.row,data.msg.col,data.msg.letter);
                        functions.checkGameEnd(games[socket.id],userdata[socket.id],function(data){
                            if(data.success){
                                socket.emit("GameEnded",false,data.info);
                            }
                        });
                    }else{
                        socket.emit("message",data.err,data.msg);
                    }
                });
             }else{
                socket.emit("message",true,"Please Make connection with Game First");
             }
        });

        socket.on('chat',function(msg){
            if(games[socket.id]!=null){
                functions.chat(games[socket.id],userdata[socket.id],msg,function(data){
                    if(!data.err){
                        socket.broadcast.to(games[socket.id]).emit("chat",data.msg);
                    }else{
                         socket.emit("message",data.err,data.msg);
                    }
                });
            }else{
                socket.emit("message",true,"Please Make connection with Game First");
             }
        });


        socket.on('disconnect', function() {
            console.log("socket disconnect");
            delete sockets[socket.id];
            socket.leave(games[socket.id]);
            delete games[socket.id];
        });
    });
}