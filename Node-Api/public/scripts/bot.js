var WordsTomake={};
var FilledSpaces=[];
var gridLength=parseInt(localStorage.gridSize);
localStorage.gridLength=parseInt(localStorage.gridSize);
var game=0;
var firstLetter=0;
var LettertoRender={};
var addedWord=[];
var score=0;
var lastletter='';
var wordCount=0;
var Row=0;
var Col=0;
var BotScore=0;
var BotAlreadyScored=[];
var LettersScore={"A": 2,"B": 4,"C": 4,"D": 3,"E": 2,"F": 5,"G": 3,"H": 5,"I": 2,"J": 9,"K": 6,"L": 2,"M": 4,"N": 2,"O": 2,"P": 4,"Q": 11,"R": 2,"S": 2,"T": 2,"U": 2,"V": 5,"W": 5,"X": 9,"Y": 5,"Z": 11};

function CalculateWidth(){
	Row=0;
	Col=0;
	var Temp=0;
	for(var i=0;i<gridLength;i++){
		Temp=0;
		for(var j=0;j<gridLength;j++){
			if(FilledSpaces.indexOf(i+""+j)==-1){
				Temp++;
			}else if(Temp<gridLength-2){
				Temp=0;
			}
		}
		if(Temp>Row){
			Row=Temp;
		}	
	}
	for(var i=0;i<gridLength;i++){
		Temp=0;
		for(var j=0;j<gridLength;j++){
			if(FilledSpaces.indexOf(j+""+i)==-1){
				Temp++;
			}else if(Temp<gridLength-2){
				Temp=0;
			}
		}
		if(Temp>Col){
			Col=Temp;
		}	
	}
}


function LetterScore(word){
    var score=0;
    if(dictionary[word[0].toUpperCase()+word.length].indexOf(word.toLowerCase())!=-1){
      var res = word.split("");
      for(var i=0;i<res.length;i++){
        score+=LettersScore[res[i].toUpperCase()];
      } 
    }
    if(word.length==gridLength-1){score*=2;}
    if(word.length==gridLength){score*=3;}
    return score;
}


function SearchSpace(letter,height,Lindex){

	if(FilledSpaces.length>((gridLength*gridLength)-height))return false;
	if(height>Col && height>Row)return false;

	letter=letter.toUpperCase();
	var word=0;
	var letterPlus=0;
	 for(var i=0;i< gridLength;i++){ 
		 for (var j=0;j<gridLength-2;j++){
			word=0;
			letterPlus=0;
		     for(var k=0;k<gridLength-j;k++){
				 var id=localStorage.getItem("Bot_"+(i)+"_"+(j+k));
				     if(!id && FilledSpaces.indexOf(i+""+(j+k))==-1){
				     	word+=1;
				     }else if(id==letter){
				     	letterPlus=1;
				     }else{
				     	word=0;
				     }
			     if((word+letterPlus)==height){
			     	return true
			     }

		     }
		}
	}

	 for(var i=0;i< gridLength;i++){ 
		 for (var j=0;j<gridLength-2;j++){
			word=0;
			letterPlus=0;
		     for(var k=0;k<gridLength-j;k++){
				 var id=localStorage.getItem("Bot_"+(j+k)+"_"+(i));
				      if(!id &&  FilledSpaces.indexOf((j+k)+""+i)==-1){
				     	word+=1;
				     }else if(id==letter){
				     	letterPlus=1;
				     }else{
				     	word=0;
				     }
				     if((word+letterPlus)==height){
				     	return true
				     }

		     }
		  }
	}

	return false;
}



function findBotScore(){
	
	var word='';
	 for(var i=0;i< gridLength;i++){ 
		 for (var j=0;j<gridLength-2;j++){
			word='';
		     for(var k=0;k<gridLength;k++){
				 var id=localStorage.getItem("Bot_"+(i)+"_"+(j+k)+" div").attr("id");
				     if(id){
				     	word+=id;
				     }else{
				     	word='';
				     }
			     if(word.length>=gridLength-2){
			     	score+=LetterScore(word);	
			     }

		     }
		  }
	}

	for(var i=0;i< gridLength;i++){ 
		for (var j=0;j<gridLength-2;j++){
			word='';
		     for(var k=0;k<gridLength;k++){
				 var id=localStorage.getItem("Bot_"+(j+k)+"_"+(i)+" div").attr("id");
				     if(id){
				     	word+=id;
				     }else{
				     	word='';
				     }
			     if(word.length>=gridLength-2){
			     	score+=LetterScore(word);	
			     }

		     }
		}
	}

}



function Eliminate(word,letter,position){
	WordsTomake[word][0].completed-=1;
	if(position){
		for(var i=0;i<WordsTomake[word][0]['letters'][letter].length;i++){
			if(position==WordsTomake[word][0]['letters'][letter][i].x){
				var xy=WordsTomake[word][0]['letters'][letter][i].x+"_"+WordsTomake[word][0]['letters'][letter][i].y;
				WordsTomake[word][0]['letters'][letter].splice(i,1);
			}
		}
	}else{
		var xy=WordsTomake[word][0]['letters'][letter][0].x+"_"+WordsTomake[word][0]['letters'][letter][0].y;
		WordsTomake[word][0]['letters'][letter].splice(0,1);
	}
	return xy;
}

function CheckEmptyBox(i,j,Passingletter){
	for(var letter in WordsTomake){
		if(WordsTomake[letter][0] && WordsTomake[letter][0]['completed']>0){
			for( var l in WordsTomake[letter][0]['letters']){	
				for(var k=0;k<WordsTomake[letter][0]['letters'][l].length;k++){
					 if(WordsTomake[letter][0]['letters'][l][k].x==i && WordsTomake[letter][0]['letters'][l][k].y==j && l!=Passingletter){
						 return false;
					}
				}
			}
		}
	}
	if(FilledSpaces.indexOf(i+""+j)!=-1){
		return false;
	}
	return true;
}


function ReserveWord(word,position){

	var row=position.x;
	var col=position.y;
	var type=position.type;
	var value=0;var tempWord=word;
	
	var WordLength=word.length;
	while(WordLength>=gridLength-2){
		addedWord.push(tempWord);
		tempWord=tempWord.slice(0,-1);
		WordLength--;
	}

	if(type=='row'){
		for(var i=col-word.length;i<col;i++){
			if(!WordsTomake[word][0]['letters'][word[value]]){
				WordsTomake[word][0]['letters'][word[value]]=[];
			}
			
			if(FilledSpaces.indexOf(row+""+i)==-1){
				WordsTomake[word][0]['letters'][word[value]].push({x:row,y:i});
				FilledSpaces.push(row+""+i);
			}else{
				WordsTomake[word][0]['completed']-=1;
			}
			
			value++;
		}
	}
	if(type=='col'){
		for(var i=row-word.length;i<row;i++){
			if(!WordsTomake[word][0]['letters'][word[value]]){
				WordsTomake[word][0]['letters'][word[value]]=[];
			}

			if(FilledSpaces.indexOf(i+""+col)==-1){
				WordsTomake[word][0]['letters'][word[value]].push({x:i,y:col});
				FilledSpaces.push(i+""+col);
			}else{
				WordsTomake[word][0]['completed']-=1;
			}
			
			value++;
		}
	}
}


function GiveWordtoUser(){

	var minimumSize=10;
	var letter;
	var word;
	var index;

	for(i in WordsTomake){
		if(WordsTomake[i][0]['completed']<minimumSize && WordsTomake[i][0]['completed']>0){
			minimumSize=WordsTomake[i][0]['completed'];
				for (j in WordsTomake[i][0]['letters']){
					if(WordsTomake[i][0]['letters'][j].length>0){
						word=i;
						letter=j;
						index=WordsTomake[i][0]['letters'][j][0].x+"_"+WordsTomake[i][0]['letters'][j][0].y;
					}
				}

		}
	}

	if(!letter){
		word=passLetter('z',false);
		letter='z';
			if(word){
				index=WordsTomake[word][0]['letters'][letter][0].x+"_"+WordsTomake[word][0]['letters'][letter][0].y;
			}
	}

	if(letter && word){
		Eliminate(word,letter);
	}else{	
		index='';
		for(var i=0;i<gridLength;i++){
			for(var j=0;j<gridLength;j++){
				if(FilledSpaces.indexOf(i+""+j)==-1 && !index){
					FilledSpaces.push(i+""+j);
					letter=letter;
					index=i+"_"+j;
					break;
					break;
				}
			}
		}
	}

	return {letter:letter,index:index};
}


function passWord(PWord,first){
	var word={};
	var string='';
	var value=0;
	var WordLength=PWord.length;

	for(var i=0;i<gridLength;i++){  	
		 for (var k=0;k<=gridLength-WordLength;k++){
			string="";
			value=0;

		     for(var j=k;j<WordLength+k;j++){
				  	var id=localStorage.getItem("Bot_"+(i)+"_"+(j)); 	
					 if(id){ string+=id.toLowerCase();}
					 else if(!CheckEmptyBox(i,j,PWord[value])){string+='0';}
					 else {string+='_';}
		     		 value++;
			}

		 	if(!word[string]){
		 		word[string]=[];
		 	}
	 		
	 		if(first){
		 		for(var local=0;local<string.length;local++){
			 			if(string[local]!="_" && string[local]!="0" ){
			 			word[string].push({x:i,y:j,type:"row",length:string.length,completed:string.length,letters:{}});
			 			break;
		 			}
		 		}
	 		}else{
	 				word[string].push({x:i,y:j,type:"row",length:string.length,completed:string.length,letters:{}});
	 		}
	}

}


	for(var i=0;i<gridLength;i++){  	
		 for (var k=0;k<=gridLength-WordLength;k++){
			string="";
			value=0;

		     for(var j=k;j<WordLength+k;j++){
			     
				  	var id=localStorage.getItem("Bot_"+(j)+"_"+(i)); 	

					 if(id){ string+=id.toLowerCase();}
					 else if(!CheckEmptyBox(j,i,PWord[value])){string+='0';}
					 else {string+='_';}

		     		value++;
			}

		 	if(!word[string]){
		 		word[string]=[];
		 	}
		 	if(first){
		 		for(var local=0;local<string.length;local++){
		 			if(string[local]!="_" && string[local]!="0" ){
		 			word[string].push({x:j,y:i,type:"col",length:string.length,completed:string.length,letters:{}});
		 			break;
		 			}
		 		}
		 	}else{
		 		word[string].push({x:j,y:i,type:"col",length:string.length,completed:string.length,letters:{}});

		 	}
		}
	}


	var count=0;
	for(i in word){
		count=0;
		for(var j=0;j<i.length;j++){
			if(first){
				if(i[j]==PWord[j]){
					count=i.length;
				}
			}
			else if(i[j]=='_'){
			count++;
			}
			if(count==i.length && addedWord.indexOf(PWord)==-1){
				return word[i][0];
			}
		}
	}
	 
	return false;
}

function passLetter(letter,first,bool){
var Mainword;
LettertoRender={};

for(var l in WordsTomake){
	if(WordsTomake[l][0]['letters'][letter]){
		if(WordsTomake[l][0]['letters'][letter].length>0){
			LettertoRender[l]=[];
			LettertoRender[l].push({completed:WordsTomake[l][0]['completed']});
		}
	}
}

var element_count = 0;
for (e in LettertoRender) { element_count++; }

	var minimumSize=10;
	for( i in LettertoRender){
		if(WordsTomake[i][0]['completed']<minimumSize){
			minimumSize=WordsTomake[i][0]['completed'];
			Mainword=i;
		}
	}


	if(element_count<=0){
	var check=true; 


	 for(var t=gridLength;t>=gridLength-2;t--){
	 	var target=[];
	 	//Getting Dictionary
		 for(var i=0;i<26;i++){
	      target[i]={}
	      target[i]= dictionary[(String.fromCharCode(65+i))+t];
	 	 }
	 	
	 	var l1;var l2;	
	 	//Search For Rows and Columns
	 	for(var i=target.length-1;i>0;i--){
	     	for(var j=0;j<target[i].length;j++){
	     		if(check){
		     		work=false;
			     		if(letter.length==2){
			     			l1=letter[0];
			     			l2=letter[1];
				     			if(target[i][j].indexOf(l1)==0 && target[i][j].indexOf(l1)!=target[i][j].slice(1).indexOf(l2) && target[i][j].slice(1).indexOf(l2)!=-1){
				     				work=true;
				     			}
			     		}else{
			     			if(target[i][j].indexOf(letter)!=-1 && SearchSpace(letter,target[i][j].length,target[i][j].indexOf(letter))){
			     				work=true;
			     			}
			     		}

		     		if(work){
		     			var word=passWord(target[i][j],first);
		     			if(word && check){
			     			WordsTomake[target[i][j]]=[];
			     			WordsTomake[target[i][j]].push(word);
			     			ReserveWord(target[i][j],word);
			     			Mainword=target[i][j];
		     				check=false;
		     				break;
		     			}

		     		}
		     	}
	    	}
	  	}
	  }
}

	if(!Mainword && bool){

		var MaxSize=0;
		var word;
			for(i in WordsTomake){
				if(WordsTomake[i][0]['completed']>MaxSize){
						MaxSize=WordsTomake[i][0]['completed'];
						for (j in WordsTomake[i][0]['letters']){
							if(WordsTomake[i][0]['letters'][j].length>0){
								word=i;
								break;
							}
						}

				}
			}

			if(word){
				WordsTomake[word][0]['completed']=-1;
				for(j in WordsTomake[word][0]['letters']){
					var length=WordsTomake[word][0]['letters'][j].length;
					for(var k=0;k<length;k++ ){
						var Sindex=WordsTomake[word][0]['letters'][j][0].x+""+WordsTomake[word][0]['letters'][j][0].y;
							var index = FilledSpaces.indexOf(Sindex);
							FilledSpaces.splice(index, 1);
							WordsTomake[word][0]['letters'][j].splice(0,1);	
					}
				}
			}
	}

	CalculateWidth();
	return Mainword;

}

function BotWork(letter) {
    letter = letter.toLowerCase();
    var word;
    var word1;
    var Obj1;
    var Obj2;
    var givenLetter;

    if (game == 0) {
        firstLetter = letter;
        game++;
    } else if (game == 1) {
        word = passLetter(firstLetter + letter, true, false);
        if (!word) {
            word = passLetter(firstLetter, true, false);
            word1 = passLetter(letter, false, false);
            // Eliminate(word, firstLetter);
            Obj2 = Eliminate(word1, letter);
        } else {
            Eliminate(word, firstLetter, 4);
            Obj1 = Eliminate(word, letter);
        }
        givenLetter = GiveWordtoUser();
        game++;
    } else {
        //Just to Empty Spaces
        word = passLetter(letter, false, true);

        if (!word) {
            word = passLetter(letter, false, false);
            if (word) {
                Obj1 = Eliminate(word, letter);
            } else {
                var index;
                for (var i = 0; i < gridLength; i++) {
                    for (var j = 0; j < gridLength; j++) {
                        if (FilledSpaces.indexOf(i + "" + j) == -1 && !index) {
                            FilledSpaces.push(i + "" + j);
                            letter = letter;
                            index = i + "_" + j;
                            Obj1 = index;
                            break;
                            break;
                        }
                    }
                }
            }
        } else {
            Obj1 = Eliminate(word, letter);
        }

        givenLetter = GiveWordtoUser();
        lastletter = letter;

    }

    if (givenLetter) {
        // console.log(givenLetter.letter);
        if (Obj1) {
            localStorage.setItem("Bot_" + Obj1, letter.toUpperCase());
        } else if (Obj2) {
            localStorage.setItem("Bot_" + Obj2, letter.toUpperCase());
        }
        localStorage.setItem("Bot_" + givenLetter.index, givenLetter.letter.toUpperCase());
        localStorage.setItem('WordsTomake', JSON.stringify(WordsTomake));
        localStorage.setItem('FilledSpaces', JSON.stringify(FilledSpaces));
        localStorage.setItem('gridSize', gridLength);
        localStorage.setItem('game', game);
        localStorage.setItem('firstLetter', firstLetter);
        localStorage.setItem('LettertoRender', JSON.stringify(LettertoRender));
        localStorage.setItem('addedWord', JSON.stringify(addedWord));
        localStorage.setItem('score', score);
        localStorage.setItem('lastletter', lastletter);
        localStorage.setItem('wordCount', wordCount);
        localStorage.setItem('Row', Row);
        localStorage.setItem('Col', Col);
        return givenLetter.letter;
    }
    FilledDataBot();
}


function BotInitialize(grsize,refresh){
  localStorage.setItem("gridSize",grsize);
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  intialGameCharacter = possible.charAt(Math.floor(Math.random() * possible.length));

  ro=Math.floor((Math.random() * grsize) + 0);
  co=Math.floor((Math.random() * grsize) + 0);
  
  localStorage.intialGameCharactercolbot=co;
  localStorage.intialGameCharacterrowbot=ro;
  localStorage.setItem("Bot_"+ro+"_"+co,intialGameCharacter);

  	if(refresh!=null){
	  	for(var i=0;i<grsize;i++){
	  		for(var j=0;j<grsize;j++){			  	
			     localStorage.setItem("Bot_"+(i)+"_"+(j),'');
			}
		}	
	}else{
		WordsTomake=JSON.parse(localStorage.getItem("WordsTomake"));
		FilledSpaces=JSON.parse(localStorage.getItem("FilledSpaces"));
		gridLength=parseInt(localStorage.getItem("gridSize"));
		game=localStorage.getItem("game");
		firstLetter=localStorage.getItem("firstLetter");
		LettertoRender=JSON.parse(localStorage.getItem("LettertoRender"));
		addedWord=JSON.parse(localStorage.getItem("addedWord"));
		score=localStorage.getItem("score");
		lastletter=localStorage.getItem("lastletter");
		wordCount=localStorage.getItem("wordCount");
		Row=localStorage.getItem("Row");
		Col=localStorage.getItem("Col");
	}
	BotWork(intialGameCharacter);
	return({letter:intialGameCharacter,row:ro,col:co});
}


function FilledDataBot() {
    BotSearch = [];
    BotIndexs = [];
    var word = '';
    gridLength = localStorage.gridSize;

    for (var i = 0; i < gridLength; i++) {

        for (var j = 0; j < gridLength - 2; j++) {
            word = '';
            for (var k = 0; k < gridLength - j; k++) {
                var id = localStorage.getItem("Bot_" + (i) + "_" + (j + k));
                if (id) {
                    word += id
                    if (word.length >= gridLength-2) {
                        if (BotAlreadyScored.indexOf(word) == -1 && BotSearch.indexOf(word) == -1) {
                            BotSearch.push(word);
                            BotIndexs.push("r_" + i + "_" + (j + k - word.length + 1) + "_" + (j + k));
                        }
                    }
                } else {
                    word = '';
                }
            }
        }
    }

    for (var i = 0; i < gridLength; i++) {
        for (var j = 0; j < gridLength - 2; j++) {
            word = '';
            for (var k = 0; k < gridLength - j; k++) {
                var id = localStorage.getItem("Bot_" + (j + k) + "_" + (i));
                if (id) {
                    word += id
                    if (word.length >= gridLength-2) {
                        if (BotAlreadyScored.indexOf(word) == -1 && BotSearch.indexOf(word) == -1) {
                            BotIndexs.push("c_" + i + "_" + (j + k - word.length + 1) + "_" + (j + k));
                            BotSearch.push(word);
                        }
                    }
                } else {
                    word = '';
                }
            }
        }
    }
    SearchAllBot();
}

function FilledDataBot() {
    BotSearch = [];
    BotIndexs = [];
    var word = '';
    gridLength = localStorage.gridSize;

    for (var i = 0; i < gridLength; i++) {
        for (var j = 0; j < gridLength - 2; j++) {
            word = '';
            for (var k = 0; k < gridLength - j; k++) {
                var id = localStorage.getItem("Bot_" + (i) + "_" + (j + k));
                if (id) {
                    word += id
                    if (word.length > 2) {
                        if (BotAlreadyScored.indexOf(word) == -1 && BotSearch.indexOf(word) == -1) {
                            BotSearch.push(word);
                            BotIndexs.push("r_" + i + "_" + (j + k - word.length + 1) + "_" + (j + k));
                        }
                    }
                } else {
                    word = '';
                }
            }
        }
    }
    for (var i = 0; i < gridLength; i++) {
        for (var j = 0; j < gridLength - 2; j++) {
            word = '';
            for (var k = 0; k < gridLength - j; k++) {
                // var id=$("#Place_"+(j+k)+"_"+(i)+" div").attr("id");
                var id = localStorage.getItem("Bot_" + (j + k) + "_" + (i));
                if (id) {
                    word += id
                    if (word.length > 2) {
                        if (BotAlreadyScored.indexOf(word) == -1 && BotSearch.indexOf(word) == -1) {
                            BotIndexs.push("c_" + i + "_" + (j + k - word.length + 1) + "_" + (j + k));
                            BotSearch.push(word);
                        }
                    }
                } else {
                    word = '';
                }
            }
        }
    }
 
    var botScore=SearchAllBot();
    localStorage.setItem('score', botScore);

}

function SearchAllBot() {
    for (var i = 0; i < BotSearch.length; i++) {
        var column = BotSearch[i][0].toUpperCase() + BotSearch[i].length;
        var variableCheck = searchWord(column, BotSearch[i]);
        if (variableCheck) {
            BotScore += findScore(BotSearch[i]);
            BotAlreadyScored.push(BotSearch[i]);
            // if (initial == 1 || initial == 2) {
                return BotScore;
                indexsToPop2.push(Indexs2[i]);
            // }
        }
    }
}


function searchWord(val, searchVal) {
    for (var i = 0; i < dictionary[val].length; i++) {
        if (dictionary[val][i] == searchVal.toLowerCase()) {
            return true;
        }
    }
    return false;
}


function findScore(word) {
    var score = 0;
    var res = word.split('');
    for (var i = 0; i < res.length; i++) {
        score += LettersScore[res[i]];
    }
    return score;
}

