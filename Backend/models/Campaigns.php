<?php

namespace Text\Models;
use Text\Common\Constants;
use Text\Models\Subscribers;
use Text\Models\CampaignsSchedules;
use Phalcon\Mvc\Model\Query\Builder as QueryBuilder;
use Exception;

class Campaigns extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $clubsId;

    /**
     *
     * @var integer
     */
    public $relatedTo;

    /**
     *
     * @var integer
     */
    public $templatesContentsId;

    /**
     *
     * @var string
     */
    public $fromName;

    /**
     *
     * @var string
     */
    public $text;

    /**
     *
     * @var string
     */
    public $imagePath;

    /**
     *
     * @var string
     */
    public $lastSentTimestamp;

    /**
     *
     * @var integer
     */
    public $currentStatus;

    /**
     *
     * @var string
     */
    public $date;

    /**
     *
     * @var string
     */
    public $time;

    /**
     *
     * @var string
     */
    public $timeZone = Constants::TIME_ZONE_SHORT_GREENWICH_MEAN_TIME;

    /**
     *
     * @var string
     */
    public $dateTimestamp;
    
    /**
     *
     * @var string
     */
    public $endDateTimestamp;

    /**
     *
     * @var string
     */
    public $nextScheduleTimestamp;

    /**
     *
     * @var string
     */
    public $scheduled = Constants::CAMPAIGN_SCHEDULED_NO;

    /**
     *
     * @var integer
     */
    public $repeatReminderCountUsed = 0;

    /**
     *
     * @var integer
     */
    public $repeatReminderCount = 0;

    /**
     *
     * @var integer
     */
    public $sendEveryCount = 0;

    /**
     *
     * @var string
     */
    public $sendEveryIntervalUnit;
    
    /**
     * Initialize foreign-key relations
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('clubsId', 'Text\Models\Clubs', 'id', array('alias' => 'Clubs'));
        $this->belongsTo('templatesContentsId', 'Text\Models\TemplatesContents', 'id', array('alias' => 'TemplatesContents'));
        $this->hasMany('id', 'Text\Models\CampaignsSubscribers', 'campaignsId', array('alias' => 'CampaignsSubscribers'));
    }

    /**
     * Add property names in $props that need to pass through
     * function applyHtmlEntityDecode when reading values from database
     */
    public function afterFetch()
    {
        parent::afterFetch();

        $props = ['fromName','text'];
        $this->applyHtmlEntityDecode($props);
    }

    /**
     * Set dateTimestamp calculated from date, time and timezone.
     * Set schedule timestamps if the scheduled field is set
     */
    public function beforeValidationOnCreate()
    {
        parent::beforeValidationOnCreate();

        $dateTimeString = $this->date . ' ' . $this->time . ' ' . $this->timeZone;
        
        $startDateTime = \DateTime::createFromFormat('m/d/Y H:i a T', $dateTimeString);

        if ($startDateTime == false) 
        {
            throw new Exception("Incorrect date/time", 1);
        }

        $this->dateTimestamp = date (Constants::TIMESTAMP_DATE_TIME_FORMAT, $startDateTime->getTimestamp());

        if ($this->scheduled == Constants::CAMPAIGN_SCHEDULED_YES) 
        {
            $intervalSeconds = Constants::TIME_INTERVAL_UNIT_SECONDS[$this->sendEveryIntervalUnit] * $this->sendEveryCount;
            $this->nextScheduleTimestamp = date(Constants::TIMESTAMP_DATE_TIME_FORMAT,$startDateTime->getTimestamp() + $intervalSeconds * ($this->repeatReminderCountUsed));

            $intervalSeconds = Constants::TIME_INTERVAL_UNIT_SECONDS[$this->sendEveryIntervalUnit] * $this->sendEveryCount;
            $this->endDateTimestamp = date(Constants::TIMESTAMP_DATE_TIME_FORMAT,$startDateTime->getTimestamp() + $intervalSeconds * ($this->repeatReminderCount - 1) + 1);
        }
    }

    /**
     * Set dateTimestamp calculated from date, time and timezone.
     * Set schedule timestamps if the scheduled field is set
     */
    public function beforeValidationOnUpdate()
    {
        parent::beforeValidationOnUpdate();
        
        $dateTimeString = $this->date . ' ' . $this->time . ' ' . $this->timeZone;

        $startDateTime = \DateTime::createFromFormat('m/d/Y H:i a T', $dateTimeString);

        if ($startDateTime == false) 
        {
            throw new Exception("Incorrect date/time", 1);
        }

        $this->dateTimestamp = date (Constants::TIMESTAMP_DATE_TIME_FORMAT, $startDateTime->getTimestamp());

        if ($this->scheduled == Constants::CAMPAIGN_SCHEDULED_YES) 
        {
            $intervalSeconds = Constants::TIME_INTERVAL_UNIT_SECONDS[$this->sendEveryIntervalUnit] * $this->sendEveryCount;
            $this->nextScheduleTimestamp = date(Constants::TIMESTAMP_DATE_TIME_FORMAT,$startDateTime->getTimestamp() + $intervalSeconds * ($this->repeatReminderCountUsed));

            $intervalSeconds = Constants::TIME_INTERVAL_UNIT_SECONDS[$this->sendEveryIntervalUnit] * $this->sendEveryCount;
            $this->endDateTimestamp = date(Constants::TIMESTAMP_DATE_TIME_FORMAT,$startDateTime->getTimestamp() + $intervalSeconds * ($this->repeatReminderCount - 1) + 1);
        }
    }

    /**
     * Returns timestamp for scheduled campaign for specified repeat $count
     *
     * @return string
     */
    public function getTimestampForScheduledCampaignForRepeatCount($count)
    {
        $dateTimeString = $this->date . ' ' . $this->time . ' ' . $this->timeZone;

        $startDateTime = \DateTime::createFromFormat('m/d/Y H:i a T', $dateTimeString);

        if ($startDateTime == false) 
        {
            throw new Exception("Incorrect date/time", 1);
        }

        if ($this->scheduled == Constants::CAMPAIGN_SCHEDULED_YES) 
        {
            $intervalSeconds = Constants::TIME_INTERVAL_UNIT_SECONDS[$this->sendEveryIntervalUnit] * $this->sendEveryCount;
            $scheduleTimestamp = date(Constants::TIMESTAMP_DATE_TIME_FORMAT,$startDateTime->getTimestamp() + $intervalSeconds * $count);

            return $scheduleTimestamp;
        }

        return null;
    }

    /**
     * Returns logo url
     *
     * @return string
     */
    public function getImageUrl()
    {
        if (isset($this->imagePath))
        {
            return $this->getDI()->getConfig()->application->publicUrl . '/' . ($this->imagePath);
        }
        
        return null;
    }

    /**
     * Deletes CampaignsSubscribers with $campaignsId as this id,
     * and deletes self, from database
     *
     * @return boolean
     */
    public function cascadeDelete()
    {
        CampaignsSubscribers::bulkDeleteByCampaignsId($this->id);
        
        return parent::cascadeDelete();
    }

    public function getMessageType()
    {
        if ($this->imagePath) 
        {
            return Constants::MESSAGE_TYPE_SHORT_MMS;;
        }
        else
        {
            return Constants::MESSAGE_TYPE_SHORT_SMS;
        }
    }

    public function isMms()
    {
        return $this->imagePath != null;
    }
    
    public function getSmsCount()
    {
        return ($this->imagePath == null) ? $this->repeatReminderCount : 0;
    }

    public function getMmsCount()
    {
        return ($this->imagePath != null) ? $this->repeatReminderCount : 0;
    }

    public function getDateTimestamp()
    {
        return strtotime($this->dateTimestamp) . '000';
    }

    public function isUpcoming()
    {
        return $this->scheduled == Constants::CAMPAIGN_SCHEDULED_YES && $this->dateTimestamp > self::getCurrentTimestamp();
    }

    public static function getCampaignByIdForUser($id,$user)
    {
        $campaigns = Campaigns::getCampaignsBuilderForUser($user)->andWhere('Text\Models\Campaigns.id='.$id)->getQuery()->execute();
        if (count($campaigns) > 0) 
        {
            return $campaigns[0];
        }

        return null;
    }

    public static function getForUser($user)
    {
        return self::query()
            ->innerJoin('Text\Models\Clubs','Text\Models\Campaigns.clubsId=Clubs.id','Clubs')
            ->innerJoin('Text\Models\UsersHasClubs','UsersHasClubs.clubsId=Clubs.id','UsersHasClubs')
            ->innerJoin('Text\Models\CampaignsSchedules','Text\Models\Campaigns.campaignsSchedulesId=CampaignsSchedules.id','CampaignsSchedules')
            ->where('UsersHasClubs.usersId='.$user->id)
            ->orderBy('CampaignsSchedules.date')
            ->execute();
    }

    public static function getCampaignsBuilderForUser($user)
    {
        $builder = new QueryBuilder(array(
            'models'=>'Text\Models\Campaigns'
        ));
        
        $builder->distinct(true);

        switch ($user->rolesId) {
            case Constants::ROLE_ID_SUPER_ADMIN:
            {
                $builder->where("true");
                break;
            }
            case Constants::ROLE_ID_ORGANIZATION_OWNER:
            case Constants::ROLE_ID_ORGANIZATION_MANAGER:
            {
                $builder->innerJoin('Text\Models\Clubs','Clubs.id=Text\Models\Campaigns.clubsId','Clubs');
                $builder->innerJoin('Text\Models\UsersHasOrganizations','UsersHasOrganizations.organizationsId=Clubs.organizationsId','UsersHasOrganizations');
                $builder->where('UsersHasOrganizations.usersId="'.$user->id.'"');

                break;
            }
                
            case Constants::ROLE_ID_CLUB_MANAGER:
            case Constants::ROLE_ID_CLUB_OWNER:
            {
                $builder->innerJoin('Text\Models\UsersHasClubs','Text\Models\Campaigns.clubsId=UsersHasClubs.clubsId','UsersHasClubs');
                $builder->where('UsersHasClubs.usersId="'.$user->id.'"');
                
                break;
            }

            default:
                throw new Exception("Invalid role", 1);
                return;
        }
        
        $builder->orderBy('Text\Models\Campaigns.modifiedDate DESC, Text\Models\Campaigns.dateTimestamp DESC');
        return $builder;
    }

    public static function getPastCampaignsBuilderForUser($user)
    {
        $builder = self::getCampaignsBuilderForUser($user);
        
        $builder->andWhere('Text\Models\Campaigns.scheduled='.Constants::CAMPAIGN_SCHEDULED_NO
                .' OR (Text\Models\Campaigns.scheduled='.Constants::CAMPAIGN_SCHEDULED_YES.' AND Text\Models\Campaigns.dateTimestamp <= NOW() AND Text\Models\Campaigns.repeatReminderCountUsed=Text\Models\Campaigns.repeatReminderCount)');
        
        return $builder;
    }

    public static function getUpcomingCampaignsBuilderForUser($user)
    {
        $builder = self::getCampaignsBuilderForUser($user);
        
        $builder->andWhere('Text\Models\Campaigns.scheduled='.Constants::CAMPAIGN_SCHEDULED_YES
                .' AND Text\Models\Campaigns.dateTimestamp > NOW()');
        
        return $builder;
    }

    public static function getRunningCampaignsBuilderForUser($user)
    {
        $builder = self::getCampaignsBuilderForUser($user);
        
        $builder->andWhere('Text\Models\Campaigns.scheduled='.Constants::CAMPAIGN_SCHEDULED_YES.
                ' AND Text\Models\Campaigns.dateTimestamp <= NOW() AND Text\Models\Campaigns.repeatReminderCountUsed<Text\Models\Campaigns.repeatReminderCount');
        
        return $builder;
    }

    public static function getPastCampaignsForUser($user)
    {
        $builder = self::getPastCampaignsBuilderForUser($user);

        return $builder->getQuery()->execute();
    }

    public static function getUpcomingCampaignsForUser($user)
    {
        $builder = self::getUpcomingCampaignsBuilderForUser($user);
        
        return $builder->getQuery()->execute();
    }

    public static function getRunningCampaignsForUser($user)
    {
        $builder = self::getRunningCampaignsBuilderForUser($user);

        return $builder->getQuery()->execute();
    }

    public static function getCampaignsForUser($user)
    {
        $builder = self::getCampaignsBuilderForUser($user);

        return $builder->getQuery()->execute();
    }

    public static function getPendingRunningCampaigns()
    {
        $builder = new QueryBuilder(array(
            'models'=>'Text\Models\Campaigns'
        ));
        
        $builder->distinct(true);

        $builder->where('Text\Models\Campaigns.scheduled='.Constants::CAMPAIGN_SCHEDULED_YES.
                ' AND Text\Models\Campaigns.dateTimestamp <= NOW() AND Text\Models\Campaigns.repeatReminderCountUsed<Text\Models\Campaigns.repeatReminderCount'.
                ' AND Text\Models\Campaigns.nextScheduleTimestamp <= NOW()');
        
        $builder->orderBy('Text\Models\Campaigns.dateTimestamp DESC');

        return $builder->getQuery()->execute();
    }

    public static function getUpomingCampaignsForOrganizationsId($organizationsId)
    {
        return self::query()
            ->innerJoin('Text\Models\Clubs','Clubs.id=Text\Models\Campaigns.clubsId','Clubs')
            ->where('Clubs.organizationsId='.$organizationsId)
            ->andWhere('Text\Models\Campaigns.scheduled='.Constants::CAMPAIGN_SCHEDULED_YES
                .' AND Text\Models\Campaigns.dateTimestamp > NOW()')
            ->orderBy('Text\Models\Campaigns.dateTimestamp DESC')
            ->execute();
    }

    public function increaseRepeatCountUsedAndScheduledDate()
    {   
        echo $this->fromName.PHP_EOL;

        $this->repeatReminderCountUsed = $this->repeatReminderCountUsed + 1;

        $this->save();
    }
}
