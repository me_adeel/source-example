<?php

namespace Text\Models;

use Text\Common\Constants;
use Text\Models\UsersHasClubs;
use Text\Models\ClubsHasSubscribers;
use Text\Models\Subscribers;

use Phalcon\Mvc\Model\Query\Builder as QueryBuilder;
use \Phalcon\Mvc\Model\Resultset\Simple as Resultset;

use Text\Exceptions\SMSLimitException;
use Text\Exceptions\MMSLimitException;

class Clubs extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $organizationsId;

    /**
     *
     * @var integer
     */
    public $active = Constants::CLUB_ACTIVE_YES;

    /**
     *
     * @var integer
     */
    public $campaignsCounter = 0;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $url;

    /**
     *
     * @var string
     */
    public $logo;

    /**
     *
     * @var string
     */
    public $mailingAddress1;

    /**
     *
     * @var string
     */
    public $mailingAddress2;

    /**
     *
     * @var string
     */
    public $mailingAddress3;

    /**
     *
     * @var string
     */
    public $city;

    /**
     *
     * @var string
     */
    public $zip;

    /**
     *
     * @var string
     */
    public $state;

    /**
     *
     * @var integer
     */
    public $smsPerMonthLimit = 0;

    /**
     *
     * @var integer
     */
    public $smsPerMonthLimitUsed = 0;

    /**
     *
     * @var integer
     */
    public $smsPerMonthLimitBase = 0;

    /**
     *
     * @var integer
     */
    public $mmsPerMonthLimit = 0;

    /**
     *
     * @var integer
     */
    public $mmsPerMonthLimitUsed = 0;

    /**
     *
     * @var integer
     */
    public $mmsPerMonthLimitBase = 0;

    /**
     *
     * @var string
     */
    public $welcomeMessage;

    /**
     *
     * @var string
     */
    public $firstName;

    /**
     *
     * @var string
     */
    public $lastName;


    /**
     *
     * @var string
     */
    public $phone;

    /**
     * Initialize foreign-key relations
     */
    public function initialize()
    {
        parent::initialize();
        
        $this->hasMany('id', 'Text\Models\Campaigns', 'clubsId', array('alias' => 'Campaigns'));
        $this->hasMany('id', 'Text\Models\ClubKeywords', 'clubsId', array('alias' => 'ClubKeywords'));
        $this->hasMany('id', 'Text\Models\ClubsHasSubscribers', 'clubsId', array('alias' => 'ClubsHasSubscribers'));
        $this->hasMany('id', 'Text\Models\UsersHasClubs', 'clubsId', array('alias' => 'UsersHasClubs'));
        $this->belongsTo('organizationsId', 'Text\Models\Organizations', 'id', array('alias' => 'Organizations'));
    }

    /**
     * Add property names in $props that need to pass through
     * function applyHtmlEntityDecode when reading values from database
     */
    public function afterFetch()
    {
        parent::afterFetch();

        $props = ['name','mailingAddress1','mailingAddress2','mailingAddress3','city','state','welcomeMessage','firstName','lastName'];
        $this->applyHtmlEntityDecode($props);
    }

    /**
     * Deletes specified models with $clubsId as this id,
     * and deletes self, from database
     *
     * @return boolean
     */
    public function cascadeDelete()
    {
        self::cascadeDeleteModels(array(
            '\Text\Models\Campaigns', 
            '\Text\Models\ClubKeywords', 
            '\Text\Models\ClubsHasSubscribers', 
            '\Text\Models\UsersHasClubs',
            '\Text\Models\SubscribersMessages'
        ),$this->id,'clubsId');
        
        return parent::cascadeDelete();
    }

    /**
     * Returns logo url
     *
     * @return string
     */
    public function getLogoPath()
    {
        if ($this->logo) 
        {
            return $this->logo;
        }

        return 'less/img/blank-profile-img.png';
    }
    
    /**
     * Returns club's managers
     *
     * @return \Text\Models\Users
     */
    public function getClubsManagers()
    {
        return Users::getClubsManagersForClubsId($this->id);
    }

    /**
     * Returns club's owners
     *
     * @return \Text\Models\Users
     */
    public function getClubsOwners()
    {
        return Users::getClubsOwnersForClubsId($this->id);
    }

    /**
     * Returns remaining sms count of total available
     *
     * @return integer
     */
    public function getSmsPerMonthLimitRemaining()
    {
        return $this->smsPerMonthLimit - $this->smsPerMonthLimitUsed;
    }
    
    /**
     * Returns remaining mms count of total available
     *
     * @return integer
     */
    public function getMmsPerMonthLimitRemaining()
    {
        return $this->mmsPerMonthLimit - $this->mmsPerMonthLimitUsed;
    }
    
    /**
     * Returns description of club as string
     * for auto suggestion search result
     *
     * @return string
     */
    public function getFormattedAutoSuggestionDescription()
    {
        return $this->getFormattedSearchResultDescription("", ", " , "");
    }

    /**
     * Returns description of club as string
     * with $prepend,$glue,$append
     *
     * @param string $prepend 
     * @param string $glue
     * @param string $append 
     * @return string
     */
    public function getFormattedSearchResultDescription($prepend="<div>",$glue="</div><div>",$append="</div>")
    {
        $values = array(
            $this->url,
            $this->mailingAddress1,
            $this->mailingAddress2,
            $this->mailingAddress3,
            $this->city,
            "<div class=\"phone-text\">".$this->phone."</div>"
        );

        return $prepend.implode($glue, array_filter($values)).$append;
    }

    /**
     * Throws exception if the specified limits cannot be assigned,
     * returns true otherwise
     *
     * @param integer $smsPerMonthLimit
     * @param integer $mmsPerMonthLimit
     * @return boolean
     */
    public function assertCanAssignLimits($smsPerMonthLimit,$mmsPerMonthLimit)
    {
        $organization = $this->organizations;
        
        $smsPerMonthLimitBaseUsed = $organization->getSmsPerMonthLimitBaseUsed() - $this->smsPerMonthLimitBase;
        $mmsPerMonthLimitBaseUsed = $organization->getMmsPerMonthLimitBaseUsed() - $this->mmsPerMonthLimitBase;

        $smsPerMonthLimitBaseRemaining = $organization->smsPerMonthLimitBase - $smsPerMonthLimitBaseUsed;
        $mmsPerMonthLimitBaseRemaining = $organization->mmsPerMonthLimitBase - $mmsPerMonthLimitBaseUsed;

        if ($smsPerMonthLimitBaseRemaining < $smsPerMonthLimit) 
        {
            throw new SMSLimitException(Constants::MESSAGE_TYPE_SHORT_SMS." limit exceeded! \"".$organization->name."\" has max ".$smsPerMonthLimitBaseRemaining." ".Constants::MESSAGE_TYPE_SHORT_SMS." to allow", 1);
        }

        if ($mmsPerMonthLimitBaseRemaining < $mmsPerMonthLimit) 
        {
            throw new MMSLimitException(Constants::MESSAGE_TYPE_SHORT_MMS." limit exceeded! \"".$organization->name."\" has max ".$mmsPerMonthLimitBaseRemaining." ".Constants::MESSAGE_TYPE_SHORT_MMS." to allow", 1);
        }

        if ($smsPerMonthLimit > $organization->smsPerMonthLimitBase) 
        {
            throw new SMSLimitException(Constants::MESSAGE_TYPE_SHORT_SMS." limit exceeded! \"".$organization->name."\" allows max ".$organization->smsPerMonthLimitBase." ".Constants::MESSAGE_TYPE_SHORT_SMS, 1);
        }

        if ($mmsPerMonthLimit > $organization->mmsPerMonthLimitBase) 
        {
            throw new MMSLimitException(Constants::MESSAGE_TYPE_SHORT_MMS." limit exceeded! \"".$organization->name."\" allows max ".$organization->mmsPerMonthLimitBase." ".Constants::MESSAGE_TYPE_SHORT_MMS, 1);
        }

        return true;
    }

    /**
     * Returns Array of Array of clubs grouped by organizations ids
     *
     * @param \Text\Models\Clubs $clubs
     * @return Array
     */
    public static function getClubsGroupedByOrganizationsIds($clubs)
    {
        $groupedClubs = array();

        foreach ($clubs as $club) 
        {
            $key = $club->organizationsId;

            if (!isset($groupedClubs[$key])) 
            {
                $groupedClubs[$key] = array();
            }

            $groupedClubs[$key][] = $club;
        }

        return $groupedClubs;
    }

    /**
     * Returns Array of Array of clubs for specified $user
     * grouped by organizations 
     *
     * @param \Text\Models\Users $user
     * @return Array
     */
    public static function getGroupsByOrganizationsIdForUser($user)
    {
        $clubs = self::getActiveClubsAssociatedWithUser($user); //self::getClubsAssociatedWithUser($user);
        
        return self::getClubsGroupedByOrganizationsIds($clubs);
    }

    /**
     * Returns clubs associated with specified user
     *
     * @param \Text\Models\Users $user
     * @return \Phalcon\Mvc\Model\Resultset\Simple
     */
    public static function getClubsForUser($user)
    {
        return self::getClubsAssociatedWithUser($user,null,null);
    }

    /**
     * Returns active clubs of active organization with $organizationId
     *
     * @param integer $organizationsId
     * @return \Phalcon\Mvc\Model\Resultset\Simple
     */
    public static function getActiveClubsWithOrganizationsId($organizationsId)
    {
        return self::query()
            ->innerJoin('Text\Models\Organizations','Text\Models\Clubs.organizationsId=Organizations.id','Organizations')
            ->where('Organizations.id='.$organizationsId)
            ->andWhere('Text\Models\Clubs.active='.Constants::CLUB_ACTIVE_YES)
            ->andWhere('Organizations.currentStatus='.Constants::ORGANIZATION_STATUS_ACTIVE)
            ->execute();
    }

    /**
     * Returns query builder of Clubs for specified user
     *
     * @param \Text\Models\Users $user
     * @return \Phalcon\Mvc\Model\Query\Builder
     */
    public static function getClubsBuilderAssociatedWithUser($user)
    {
        $builder = new QueryBuilder(array(
            'models'=>'Text\Models\Clubs'
        ));
        
        $builder->distinct(true);

        $builder->innerJoin('Text\Models\Organizations','Organizations.id=Text\Models\Clubs.organizationsId','Organizations');
        if ($user->isSuperAdmin()) 
        {
            
        }
        else if ($user->isOrganizationOwnerOrManager()) 
        {
            $builder->innerJoin('Text\Models\UsersHasOrganizations','UsersHasOrganizations.organizationsId=Text\Models\Clubs.organizationsId','UsersHasOrganizations')
                ->where('UsersHasOrganizations.usersId='.$user->id);
        }
        else
        {
            $builder->innerJoin('Text\Models\UsersHasClubs','UsersHasClubs.clubsId=Text\Models\Clubs.id','UsersHasClubs')
            ->where('UsersHasClubs.usersId='.$user->id);
        }

        return $builder;
    }

    /**
     * Returns true if specified user has associated clubs,
     * returns false otherwise
     *
     * @param \Text\Models\Users $user
     * @return boolean
     */
    public static function hasClubsAssociatedWithUser($user)
    {
        return count(self::getClubsAssociatedWithUser($user,1)) > 0;
    }
    
    /**
     * Returns true if specified user has associated active clubs
     * in active organizations, returns false otherwise
     *
     * @param \Text\Models\Users $user
     * @return boolean
     */
    public static function hasActiveClubsAssociatedWithUser($user)
    {
        return count(self::getClubsAssociatedWithUser($user,1,Constants::ORGANIZATION_STATUS_ACTIVE,Constants::CLUB_ACTIVE_YES)) > 0;
    }
    
    /**
     * Returns clubs associated with specified user,
     * with result $limit, club $status, organization $active.
     *
     * @param \Text\Models\Users $user
     * @param integer $limit
     * @param integer $status
     * @param integer $active
     * @return \Phalcon\Mvc\Model\Resultset\Simple
     */
    public static function getClubsAssociatedWithUser($user,$limit=null,$status=null,$active=Constants::CLUB_ACTIVE_YES)
    {
        $builder = self::getClubsBuilderAssociatedWithUser($user);

        if (isset($status)) 
        {
            $builder->andWhere('Organizations.currentStatus='.$status);
        }

        if (isset($active)) 
        {
            $builder->andWhere('Text\Models\Clubs.active='.$active);
        }

        if (isset($limit))
        {
            $builder->limit($limit);
        }

        $clubs = $builder->getQuery()->execute();

        return $clubs;
    }

    /**
     * Returns active clubs of active organizations, associated with specified user,
     * with result $limit
     *
     * @param \Text\Models\Users $user
     * @param integer $limit
     * @return \Phalcon\Mvc\Model\Resultset\Simple
     */
    public static function getActiveClubsAssociatedWithUser($user,$limit=null)
    {
        return self::getClubsAssociatedWithUser($user,$limit,Constants::ORGANIZATION_STATUS_ACTIVE,Constants::CLUB_ACTIVE_YES);
    }

    /**
     * Returns club with specified $id for specified $user
     *
     * @param \Text\Models\Users $user
     * @param integer $id
     * @return \Text\Models\Clubs
     */
    public static function findFirstByIdForUser($user,$id)
    {
        $rolesId = $user->rolesId;
        if ($rolesId == Constants::ROLE_ID_SUPER_ADMIN) 
        {
            return self::findFirstById($id);
        }
        
        $clubsQuery = self::query();
        if ($rolesId == Constants::ROLE_ID_ORGANIZATION_OWNER 
            || $rolesId == Constants::ROLE_ID_ORGANIZATION_MANAGER)
        {
            $clubsQuery->innerJoin('Text\Models\UsersHasOrganizations','UsersHasOrganizations.organizationsId=Text\Models\Clubs.organizationsId','UsersHasOrganizations')
            ->where('UsersHasOrganizations.usersId='.$user->id)
            ->andWhere('Text\Models\Clubs.id='.$id);
        }
        else
        {
            $clubsQuery->innerJoin('Text\Models\UsersHasClubs','UsersHasClubs.clubsId=Text\Models\Clubs.id','UsersHasClubs')
            ->innerJoin('Text\Models\Organizations','Organizations.id=Text\Models\Clubs.organizationsId','Organizations')
            ->where('UsersHasClubs.usersId='.$user->id)
            ->andWhere('Text\Models\Clubs.id='.$id)
            ->andWhere('Text\Models\Clubs.active='.Constants::CLUB_ACTIVE_YES)
            ->andWhere('Organizations.currentStatus='.Constants::ORGANIZATION_STATUS_ACTIVE);
        }
        $clubs = $clubsQuery->execute();

        return count($clubs) > 0 ? $clubs[0] : null;
    }

    /**
     * Deducts $sms and $mms limits from organization of club with $clubsId.
     * If $smsPayback is true then tries to revert the used sms limit.
     * If $mmsPayback is true then tries to revert the used mms limit.
     * If $force is true then allows limits to go negative
     * Thows error if limit cannot be assigned.
     *
     * @param integer $clubsId
     * @param integer $sms
     * @param integer $mms
     * @param boolean $smsPayback
     * @param boolean $mmsPayback
     * @param boolean $force
     * @return \Text\Models\Clubs
     */
    public static function deductSmsAndMmsLimitsFromOrganizationOfClubWithId($clubsId,$sms,$mms,$smsPayback=false,$mmsPayback=false,$force=false)
    {
        $club = self::findFirstById($clubsId);
        Organizations::deductSmsAndMmsLimitsFromOrganizationWithId($club->organizationsId,$sms,$mms,$smsPayback,$mmsPayback,$force);

        $club->smsPerMonthLimitUsed += $sms;
        $club->mmsPerMonthLimitUsed += $mms;
        $club->save();

        return $club;
    }

    /**
     * Returns the count of subscribers currently opted-in to this club
     * @return integer
     */
    public function getActiveSubscribersCount() 
    {
        return ClubsHasSubscribers::count(
            "clubsId=".$this->id." AND deleted=".Constants::DELETED_NO
        );
    }
}
