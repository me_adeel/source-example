<?php
namespace Text\Controllers;

use Text\Models\Organizations;
use Text\Models\Clubs;
use Text\Models\Users;
use Text\Models\Files;
use Text\Models\UsersHasClubs;
use Text\Models\ClubKeywords;

use Text\Common\Constants;
use Text\Forms\ClubsForm;

use Phalcon\Tag;

use Phalcon\Paginator\Adapter\QueryBuilder as Paginator;

use \Exception;
use Text\Exceptions\EmailRoleMismatchException;
use Text\Exceptions\KeywordException;
use Text\Exceptions\SMSLimitException;
use Text\Exceptions\MMSLimitException;

/**
 * CRUD to manage clubs
 */
class ClubsController extends ControllerBase
{

    /**
     * Set the private (authenticated) layout (layouts/private.volt)
     */
    public function initialize()
    {
        parent::initialize();

        $this->view->setTemplateBefore('private');
    }

    /**
     * List clubs using $builder.
     * If no item then print message $messageOnEmpty
     *
     * @param \Phalcon\Mvc\Model\Query\Builder $builder
     * @param string $messageOnEmpty
     */
    public function paginateClubsWithBuilder($builder,$messageOnEmpty)
    {
        $limit = 8;
        $numberPage = $this->request->getQuery("page", "int");

        $paginator = new Paginator(array(
            "builder" => $builder,
            "limit" => $limit,
            "page" => $numberPage
        ));

        $page = $paginator->getPaginate();
        
        if (count($page->items) == 0) 
        {
            $this->flash->notice(Constants::FLASH_MSG_PART_OOOPS ." ".$messageOnEmpty);
        }

        $this->view->page = $page;
        $this->view->clubs= $page->items;
        $this->view->each = $limit;
    }
    
    /**
     * Common function for creating/retrieving user for $club
     * Used by add and json_addUser actions.
     *
     * @param integer $rolesId
     * @param \Text\Models\Clubs $club
     * @return \Text\Models\Users
     */
    private function postRequestGetOrAddClubUserWithRolesId($rolesId,$club)
    {
        $email = null;

        if ($rolesId == Constants::ROLE_ID_CLUB_OWNER)
        {
            $email = $this->request->getPost('owner-email','string');    
        }

        if (!isset($email))
        {
            $email = $this->request->getPost('user-email','string');
        }
                    
        $user = Users::findFirstByEmail($email);

        if ($user == null) 
        {
            $user = new Users();

            $user->firstName = $this->request->getPost('user-first-name','string');
            $user->lastName  = $this->request->getPost('user-last-name','string');
            $user->email     = $email;
            $user->phone     = $this->request->getPost('user-phone','string');
            $user->rolesId   = $rolesId;

            if ($club) 
            {
                $user->signupMessage = $club->name . " has assigned you as " . Constants::ROLE_NAMES[$rolesId];
                $user->signupInfoMessage = "Please validate your email below and change your password upon first login";
            }
            
            if (!$user->save())
            {
                // $this->flash->error($user->getMessages());
                throw new Exception("Failed to create user", 1);
            }
        }

        if ($user->rolesId != $rolesId) 
        {
            throw new EmailRoleMismatchException(Constants::ERROR_MSG_FORM_EMAIL_ROLE_MISMATCH, 1);
        }

        return $user;
    }

    /**
     * Default action
     */
    public function indexAction()
    {
        $builder = Clubs::getClubsBuilderAssociatedWithUser($this->auth->getUser(),null,null,null);

        $this->paginateClubsWithBuilder($builder, "There are no clubs available.");
    }

    /**
     * Adds new clubs to an organization
     */
    public function addAction()
    {
        $currentUser = $this->auth->getUser();
        if($currentUser->isClubOwnerOrManager())
        {
            return $this->response->redirect('dashboard');
        }

        $form = new ClubsForm(null,array('add-club'=>true));
        
        if ($this->request->isPost()) 
        {
            if($this->isValid($form))
            {
                $smsPerMonthLimit = 0;
                $mmsPerMonthLimit = 0;
                
                try
                {
                    $smsPerMonthLimit = $this->request->getPost('text-messages-per-month-limit','int',0);
                    $mmsPerMonthLimit = $this->request->getPost('images-per-month-limit','int',0);
                    
                    $organizationsId = $this->request->getPost('organization-id','string');
                    
                    $organization = Organizations::findFirstByIdForUser($currentUser,$organizationsId);

                    if ($organization == null) 
                    {
                        throw new Exception("Specified organization not found", 1);
                    }
                    
                    $club = new Clubs();
                    $club->organizationsId  = $organizationsId;

                    $finishAddClubSubmit = $this->request->getPost('finish-add-club-submit','string');
                    $continueAddClubSubmit = $this->request->getPost('continue-add-club-submit','string');
                    $AddNewUserSubmit = $this->request->getPost('add-new-user-submit','string');

                    if ($finishAddClubSubmit == null && $continueAddClubSubmit == null && $AddNewUserSubmit == null) 
                    {
                        throw new Exception("You can either finish, continue to add club or continue to add user", 1);
                    }

                    $_POST['user-first-name'] = $this->request->getPost('owner-first-name','string');
                    $_POST['user-last-name'] = $this->request->getPost('owner-last-name','string');
					$_POST['user-phone'] = $this->request->getPost('owner-phone','string');

                    $keyword = $this->request->getPost('club-keyword','string');
                    $clubKeyword = ClubKeywords::findFirstByKeyword($keyword);
                    
                    if ($clubKeyword != null) 
                    {
                        throw new KeywordException(Constants::ERROR_MSG_FORM_KEYWORD_UNAVAILABLE, 1);
                    }

                    $club->firstName        = $this->request->getPost('owner-first-name','string');
                    $club->lastName         = $this->request->getPost('owner-last-name','string');
                    $club->phone            = $this->request->getPost('phone','string');
                    $club->name             = $this->request->getPost('club-name','string');
                    $club->url              = $this->request->getPost('club-url','string');
                    $club->mailingAddress1  = $this->request->getPost('mailing-address-1','string');
                    $club->mailingAddress2  = $this->request->getPost('mailing-address-2','string');
                    $club->mailingAddress3  = $this->request->getPost('mailing-address-3','string');
                    $club->city             = $this->request->getPost('club-city','string');
                    $club->state            = $this->request->getPost('club-state','string');
                    $club->zip              = $this->request->getPost('club-zip','string');
                    // $club->smsPerMonthLimit = $club->smsPerMonthLimitBase = $smsPerMonthLimit;
                    // $club->mmsPerMonthLimit = $club->mmsPerMonthLimitBase = $mmsPerMonthLimit;
                    $club->welcomeMessage   = $this->request->getPost('club-welcome-message','string');
                    if ($club->welcomeMessage > 160) 
                    {
                        throw new Exception("Welcome message cannot be greater than 160 characters", 1);
                    }
                    
                    $file = null;
                    if ($this->request->hasFiles() == true) 
                    {
                        $postFile = $this->request->getUploadedFiles()[0];
                        
                        $file = Files::createNew($postFile);
                        $club->logo = $file->path;
                    }

                    if (!$club->save())
                    {
                        throw new Exception("Failed to save club", 1);
                    }

                    // add club keyword
                    $clubKeyword = new ClubKeywords();
                    $clubKeyword->keyword = $keyword;
                    $clubKeyword->clubsId = $club->id;
                    
                    if(!$clubKeyword->save())
                    {
                        $club->delete();
                        throw new Exception("Failed to save keyword", 1);
                    }

                    try 
                    {
                        $user = $this->postRequestGetOrAddClubUserWithRolesId(Constants::ROLE_ID_CLUB_OWNER,$club);
                    }
                    catch (EmailRoleMismatchException $e)
                    {
                     	$clubKeyword->delete();
                       	$club->delete();
                        
                        throw new EmailRoleMismatchException($e->getMessage());
                    }
                    catch (Exception $e) 
                    {
                        //$clubKeyword->delete();
                        //$club->delete();

                        //Organizations::addSmsAndMmsLimitsToOrganizationWithId($organization->id,$smsPerMonthLimit,$mmsPerMonthLimit);
                        //throw new Exception("Failed to assign user", 1);
                    }
                    
                    try 
                    {
						if($user != NULL)
                        $usersHasClubs = UsersHasClubs::assignClubToUser($club,$user);
                    }
                    catch (Exception $e) 
                    {
                        throw new Exception($e->getMessage(), 1);
                        
                        // throw new Exception(Constants::FLASH_MSG_PART_ERROR." Failed to add user to the club.", 1);
                    }
                    
                    
                    Tag::resetInput();

                    $this->flashSession->success(Constants::FLASH_MSG_PART_SUCCESS . ' Club added.');
                    if (isset($finishAddClubSubmit))
                    {
                        return $this->response->redirect('dashboard');
                    }
                    else if(isset($continueAddClubSubmit))
                    {
                        return $this->response->redirect('clubs/add?orgid='.$club->organizationsId);
                    }
                    else if (isset($AddNewUserSubmit))
                    {
                        return $this->response->redirect('clubs/edit?id='.$club->id);
                    }
                }
                // catch(SMSLimitException $e)
                // {
                //     $this->showFormRequestErrorMessage();
                //     $form->setPriorityMessageFor('text-messages-per-month-limit', $e->getMessage());
                // }
                // catch(MMSLimitException $e)
                // {
                //     $this->showFormRequestErrorMessage();
                //     $form->setPriorityMessageFor('images-per-month-limit', $e->getMessage());
                // }
                catch(KeywordException $e)
                {
                    $this->showFormRequestErrorMessage();
                    $form->setPriorityMessageFor('club-keyword', $e->getMessage());
                }
                catch(EmailRoleMismatchException $e)
                {
                    $this->showFormRequestErrorMessage();
                    $form->setPriorityMessageFor('owner-email', $e->getMessage());
                }
                catch(KeywordException $e)
                {
                    $this->showFormRequestErrorMessage();
                    $form->setPriorityMessageFor('club-keyword', $e->getMessage());
                }
                catch(Exception $e)
                {
                    $this->flash->error($e->getMessage());
                }
            }
        }

        $id = $this->request->getQuery("orgid", "int", "-1");
        $step = $this->request->getQuery("step", "string");

        $organization = null;
        try 
        {
            $organization = Organizations::findFirstByIdForUser($currentUser,$id);

            if ($organization == null) 
            {
                return $this->response->redirect('dashboard');
            }
        }
        catch (Exception $e)
        {
            return $this->response->redirect('dashboard');
        }

        $form->get('organization-id')->setDefault($id);

        $this->view->organization = $organization;
        $this->view->step = $step;
        $this->view->form = $form;
    }
    /**
     * Edits existing club information
     */
    public function editAction()
    { 
        $currentUser = $this->auth->getUser();
        $canEditLimits = $currentUser->canEditLimits();
        $canEditClubActive = $currentUser->canEditClubActive();
        $canEditClubWelcomeMessage = $currentUser->canEditClubWelcomeMessage();
        $formSubmit = null;

        if ($this->request->isPost()) 
        {
            $formSubmit = new ClubsForm(null,
                array('edit-club'=>true,
                    'can-edit-limits'=>$canEditLimits,
                    'isAdmin'=>$this->view->isAdmin,
                    'can-edit-welcome-message'=>$canEditClubWelcomeMessage
                    )
                );
            if($this->isValid($formSubmit))
            {
                try
                {
                    $clubsId = $this->request->getPost('club-id','string');
                    $club = null;
                    if (isset($clubsId)) 
                    {
                        $club = Clubs::findFirstByIdForUser($this->auth->getUser(),$clubsId);
                    }
                    
                    if ($club == null) 
                    {
                        throw new Exception("No club found", 1);
                    }

                    $newSmsPerMonthLimit = $this->request->getPost('text-messages-per-month-limit','int',0);
                    $newMmsPerMonthLimit = $this->request->getPost('images-per-month-limit','int',0);

                    $keyword = $this->request->getPost('club-keyword','string');
                    $clubKeyword = ClubKeywords::findFirstByKeyword($keyword);
                    
                    if ($clubKeyword != null) 
                    {
                        if($clubKeyword->clubsId != $club->id)
                        {
                            throw new KeywordException(Constants::ERROR_MSG_FORM_KEYWORD_UNAVAILABLE, 1);
                        }
                    }
                    else
                    {
                        $clubKeywords = ClubKeywords::findByClubsId($club->id);
                        foreach ($clubKeywords as $key => $clubKeyword) 
                        {
                            $clubKeyword->delete();
                        }

                        $clubKeyword = new ClubKeywords();
                        $clubKeyword->clubsId = $club->id;
                        $clubKeyword->keyword = $keyword;

                        if (!$clubKeyword->save()) 
                        {                            
                            throw new Exception("Could not save club keyword", 1);
                        }
                    }

                    $club->phone            = $this->request->getPost('phone','string');
                    $club->name             = $this->request->getPost('club-name','string');
                    $club->url              = $this->request->getPost('club-url','string');
                    $club->mailingAddress1  = $this->request->getPost('mailing-address-1','string');
                    $club->mailingAddress2  = $this->request->getPost('mailing-address-2','string');
                    $club->mailingAddress3  = $this->request->getPost('mailing-address-3','string');
                    $club->city             = $this->request->getPost('club-city','string');
                    $club->state            = $this->request->getPost('club-state','string');
                    $club->zip              = $this->request->getPost('club-zip','string');
                    
                    if ($canEditClubActive)
                    {
                        $active = $this->request->getPost('club-activate-check','string');
                        $club->active = isset($active) ? Constants::CLUB_ACTIVE_YES : Constants::CLUB_ACTIVE_NO;
                    }
                    if ($canEditClubWelcomeMessage) 
                    {
                        $club->welcomeMessage   = $this->request->getPost('club-welcome-message','string');
                    }
                    
                    $file = null;
                    if ($this->request->hasFiles() == true) 
                    {
                        $postFile = $this->request->getUploadedFiles()[0];
                        
                        $file = Files::createNew($postFile);
                        $club->logo = $file->path;
                    }

                    if(!$club->save())
                    {
                        $clubKeyword->delete();
                        
                        throw new Exception("Could not save club", 1);
                    }

                    $this->flash->success(Constants::FLASH_MSG_PART_SUCCESS.' Club information updated.');
                }
                // catch(SMSLimitException $e)
                // {
                //     $this->showFormRequestErrorMessage();
                //     $formSubmit->setPriorityMessageFor('text-messages-per-month-limit', $e->getMessage());
                // }
                // catch(MMSLimitException $e)
                // {
                //     $this->showFormRequestErrorMessage();
                //     $formSubmit->setPriorityMessageFor('images-per-month-limit', $e->getMessage());
                // }
                catch(KeywordException $e)
                {
                    $this->showFormRequestErrorMessage();
                    $formSubmit->setPriorityMessageFor('club-keyword', $e->getMessage());
                }
                catch(Exception $e)
                {
                    $thisSubmit->flash->error($e->getMessage());
                }
            }
        }
        
        $form = new ClubsForm(null,
            array('edit-club'=>true,
                'can-edit-limits'=>true,
                'isAdmin'=>$this->view->isAdmin,
                'can-edit-welcome-message'=>true
                )
            );

        $id = $this->request->getQuery("id", "int", 0);

        $club = null;
        try 
        {
            $club = Clubs::findFirstByIdForUser($this->auth->getUser(),$id);

            if ($club == null) 
            {
                return $this->response->redirect('dashboard');
            }            
        }
        catch (Exception $e)
        {
            return $this->response->redirect('dashboard');
        }
        
        $clubUserForm = new ClubsForm(null,array('add-user'=>true));
        $clubUserForm->get('club-id')->setDefault($id);

        $form->get('club-id')->setDefault($id);

        $owners = $club->getClubsOwners();
        $owner = null;
        if (count($owners) > 0) 
        {
            $owner = $owners[0];
        }

        $firstName = null; 
        $lastName = null; 
        $email = null;
        $phone = null;

        if ($owner != null)
        {
            $firstName = $owner->firstName;
            $lastName  = $owner->lastName;
            $email     = $owner->email;
            $phone     = $owner->phone;
        }

        $clubKeyword = ClubKeywords::findFirstByClubsId($club->id);
        $keyword = $clubKeyword ? $clubKeyword->keyword : null;
        
        $form->bind(array(
                'phone'               => $club->phone,
                'mailing-address-1'   => $club->mailingAddress1,
                'mailing-address-2'   => $club->mailingAddress2,
                'mailing-address-3'   => $club->mailingAddress3,
                'club-city'           => $club->city,
                'club-state'          => $club->state,
                'club-zip'            => $club->zip,
                'club-name'           => $club->name,
                'club-url'            => $club->url,
                'club-keyword'        => $keyword,
                'club-welcome-message' => $club->welcomeMessage,
                'text-messages-per-month-limit' => $club->smsPerMonthLimitBase,
                'images-per-month-limit' => $club->mmsPerMonthLimitBase,
            ), $club
        );

        $this->view->form = $form;
        $this->view->formSubmit = $formSubmit != null ? $formSubmit : $form;
        $this->view->clubUserForm = $clubUserForm;

        $this->view->canEditLimits = $canEditLimits;
        $this->view->canEditClubActive = $canEditClubActive;
        $this->view->canEditClubWelcomeMessage = $canEditClubWelcomeMessage;
        $this->view->canViewProfile = $currentUser->isSuperAdmin();
        $this->view->canAddRemoveUsers = $currentUser->isSuperAdminOrOrganizationOwner();

        $this->view->club = $club;
        $this->view->originalKeyword = $keyword ? $keyword : "";
    }
    
    /**
     * Adds user to existing club
     */
    public function json_addUserAction()
    {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $this->response->setHeader('Content-type','application/json');

        $form = new ClubsForm(null,array('add-user'=>true));

        $result = array();
        $messages = [];

        if ($this->request->isPost()) 
        {
            if ($form->isValid($this->request->getPost()) != false) 
            {
                try
                {
                    $roleId = $this->request->getPost('user-role-select','int');

                    if (!isset($roleId) || ($roleId != Constants::ROLE_ID_CLUB_OWNER && $roleId != Constants::ROLE_ID_CLUB_MANAGER)) 
                    {
                        throw new Exception("Invalid role", 1);
                    }

                    $club = Clubs::findFirstByIdForUser($this->auth->getUser(),$this->request->getPost('club-id','string'));
                    $user = $this->postRequestGetOrAddClubUserWithRolesId($roleId,$club);

                    $usersHasClubs = UsersHasClubs::assignClubToUser($club,$user);
                }
                catch(EmailRoleMismatchException $e)
                {
                    $key = 'user-email';

                    if(!isset($messages[$key]))
                    {
                        $messages[$key] = array();
                    }

                    $messages[$key][] = $e->getMessage();
                }
                catch (Exception $e) 
                {
                    $messages[] = array($e->getMessage());
                }
            }
            else
            {
                foreach($form->getMessages() as $message) 
                {
                    $key = $message->getField();
                    
                    if(!isset($messages[$key]))
                    {
                        $messages[$key] = array();
                    }

                    $messages[$key][] = $message->getMessage();
                }
            }

            if (count($messages) > 0) 
            {
                $result['code'] = 400;
                $messages['main-messages'] = array(Constants::ERROR_MSG_FORM_REQUEST);
            }
            else
            {
                $result['code'] = 200;
                $messages['main-messages'] = array("User added successfully");
            }
        }
        else
        {
            $result['code'] = 404;
        }

        $result['messages'] = $messages;

        echo json_encode( $result );
    }

    /**
     * Remves user with $userId from club with $id
     *
     * @param integer $id
     * @param integer $userId
     */
    public function removeUserAction($id,$userId)
    {
        $currentUser = $this->auth->getUser();
        
        if (!$currentUser->canRemoveClubUsers() )
        {
            $this->flashSession->error(Constants::ERROR_MSG_GENERAL_UNAUTHORIZED_ACTION);

            return $this->response->redirect('dashboard');
        }

        $usersHasClubs = UsersHasClubs::getFirstUsersHasClubsWithUsersIdAndClubsId($userId,$id);

        if ($usersHasClubs) 
        {
            $usersHasClubs->delete();
            $this->flashSession->success(Constants::FLASH_MSG_PART_SUCCESS." User removed from the club.");
        }
        else
        {
            $this->flashSession->error(Constants::FLASH_MSG_PART_OOOPS." User not found.");
        }

        return $this->response->redirect('clubs/edit?id='.$id);
    }
}
