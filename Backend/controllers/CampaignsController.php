<?php
namespace Text\Controllers;

use Text\Forms\CampaignsForm;
use Text\Models\Clubs;
use Text\Models\Organizations;
use Text\Models\UsersHasClubs;
use Text\Models\Campaigns;
use Text\Models\CampaignsSubscribers;
use Text\Models\Subscribers;
use Text\Models\SubscribersMessages;
use Text\Models\SubscribersMessagesContents;
use Text\Models\ClubsHasSubscribers;
use Text\Models\TemplatesContents;
use Text\Models\Templates;
use Text\Models\Sms;
use Text\Common\Constants;
use Text\Common\ResourceManager;
use Text\Models\Files;

use Phalcon\Tag;
use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\QueryBuilder as Paginator;

use Exception;
use Text\Exceptions\ClubSelectionException;

require_once ($config->application->twilioDir . 'Twilio.php');

/**
 * CRUD to manage Campaigns.
 */
class CampaignsController extends ControllerBase
{

    /**
     * Set the private (authenticated) layout (layouts/private.volt)
     */
    public function initialize()
    {
        parent::initialize();

        $this->view->setTemplateBefore('private');

        if (!$this->hasAssociatedActiveClubs) 
        {
            $this->flash->notice("There must be at least one organization and one club to create and manage campaigns.");

            return $this->dispatcher->forward(
                array(
                    'controller'=>'campaigns',
                    'action'=>'index'
                )
            );
        }
    }

    /**
     * Default action, shows campaigns
     */
    public function indexAction()
    {
        if ($this->hasAssociatedActiveClubs) 
        {
            return $this->response->redirect('campaigns/running');
        }
    }

    /**
     * Shows upcoming campaigns page
     */
    public function upcomingAction()
    {
        $user = $this->auth->getUser();
        
        $campaigns = Campaigns::getUpcomingCampaignsBuilderForUser($user);

        $this->listCampaigns($campaigns);
    }

    /**
     * Shows past campaigns page
     */
    public function pastAction()
    {
        $user = $this->auth->getUser();
        
        $campaigns = Campaigns::getPastCampaignsBuilderForUser($user);

        $this->listCampaigns($campaigns);
    }

    /**
     * Shows all campaigns page
     */
    public function viewAction()
    {
        $user = $this->auth->getUser();
        
        $campaigns = Campaigns::getCampaignsBuilderForUser($user);

        $this->listCampaigns($campaigns);
    }

    /**
     * Shows running campaigns page
     */
    public function runningAction()
    {
        $user = $this->auth->getUser();
        
        $campaigns = Campaigns::getRunningCampaignsBuilderForUser($user);

        $this->listCampaigns($campaigns);
    }

    /**
     * Common function for listing campaigns, and handling
     * CampaignsForm submission;
     * Paginates campaigns based on $builder
     *
     * @param \Phalcon\Mvc\Model\Query\Builder $builder
     */
    private function listCampaigns($builder)
    {
        $form = new CampaignsForm(null,array('campaigns-list'=>true));

        $numberPage = 1;

        if ($this->request->isPost()) 
        {
            $deleteSubmit = $this->request->getPost('delete-campaign-submit');
            if (isset($deleteSubmit)) 
            {
                return $this->dispatcher->forward( array(
                    'controller' => 'campaigns',
                    'action' => 'delete',
                    'params' => array('type' => $deleteSubmit)
                ));
            }

            $query = Criteria::fromInput($this->di, 'Text\Models\Campaigns', $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        }
        else 
        {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $paginator = new Paginator(array(
            "builder" => $builder,
            "limit" => 10,
            "page" => $numberPage
        ));

        $page = $paginator->getPaginate();
        if (count($page->items) == 0) 
        {
            $this->flash->notice(Constants::FLASH_MSG_PART_OOOPS." There are no campaigns.");
        }

        $this->view->page = $page;
        $this->view->form = $form;
    }

    /**
     * Deletes campaigns
     */
    public function deleteAction($pageType=null) 
    {
        if ($this->request->isPost()) 
        {
            $currentUser = $this->auth->getUser();
            
            $campaignIds = $this->request->getPost('campaign-ids','string');

            foreach ($campaignIds as $campaignId) 
            {
                $campaign = Campaigns::getCampaignByIdForUser($campaignId,$currentUser);
                if ($campaign != null) 
                {
                    $campaign->cascadeDelete();
                }
            }
            
            
            if (isset($pageType)) 
            {
                if (isset($campaignIds))
                {
                    $campaignsCount = count($campaignIds);
                    $this->flashSession->success(Constants::FLASH_MSG_PART_SUCCESS . " " . $campaignsCount. " campaign" . ($campaignsCount > 1 ? 's' : '') . " deleted.");
                }
                else
                {
                    $this->flashSession->error(Constants::FLASH_MSG_PART_OOOPS . " You didn't select any campaign to delete.");
                }
                return $this->response->redirect('campaigns/'.$pageType);
            }        
        }
        
        return $this->response->redirect('campaigns/calendar');
    }

    /**
     * Todo: (adeel,kunwar)
     */
	public function deletecurrentAction($pageType=null) 
    {
        if ($this->request->isPost()) 
        {
            $currentUser = $this->auth->getUser();
            
            $campaignId= $this->request->getPost('campaign-id','string');
            
            $campaign = Campaigns::getCampaignByIdForUser($campaignId,$currentUser);
            if ($campaign != null) 
            {
                $campaign->cascadeDelete();
            }
            
            if (isset($pageType)) 
            {
                if (isset($campaignId))
                {
                    $campaignsCount = count($campaignId);
                    $this->flashSession->success(Constants::FLASH_MSG_PART_SUCCESS . " " . $campaignsCount. " campaign" . ($campaignsCount > 1 ? 's' : '') . " deleted.");
                }
                else
                {
                    $this->flashSession->error(Constants::FLASH_MSG_PART_OOOPS . " You didn't select any campaign to delete.");
                }
                return $this->response->redirect('campaigns/'.$pageType);
            }        
        }
        
        return $this->response->redirect('campaigns/calendar');
    
    }

    /**
     * Shows calendar page
     */
    public function calendarAction()
    {

    }

    /**
     * Compose a new campaign
     */
    public function composeAction()
    {
        $id = $this->request->getQuery("id", "int", -1);

        if ($id != -1 && Campaigns::findFirstById($id) == null)
        {
            return $this->response->redirect('campaigns/compose');
        }

        $this->composeWithEdit(false);
    
        $currentUser = $this->auth->getUser();
        if ($id != -1 && !$this->request->isPost()) 
        {
            $currentUser = $this->auth->getUser();
            $campaign = Campaigns::getCampaignByIdForUser($id,$currentUser);
            if ($campaign == null) 
            {
                return $this->response->redirect('campaigns/compose');
            }
            
            $this->view->form->bind(array(
                    'from-name'     => $campaign->fromName,
                    'text'          => $campaign->text,
                    'schedule-date' => $campaign->date,
                    'event-time'    => $campaign->time,
                    'send-every'    => $campaign->sendEveryCount
                ), $campaign
            );

            $repeatReminderCount = $campaign->repeatReminderCount;
            $this->view->form->get('repeat-reminder-select')->setDefault($repeatReminderCount . ($repeatReminderCount>1?"times":"time"));
            $this->view->form->get('time-zone-select')->setDefault($campaign->timeZone);

            $this->view->sendEveryRadioChecked = $campaign->sendEveryIntervalUnit;
            $this->view->checkedClubIds = array(
                $campaign->clubsId => true
            );
            
            $this->view->campaign = $campaign;
        }
        else if (!$this->request->isPost())
        {
            $this->view->timezoneIndex = -1;
        }

        $this->view->templates = Templates::getTemplatesForUser($currentUser);//Templates::findByCreatedBy($this->auth->getUser()->id);
    }

    /**
     * Edit a campaign
     */
    public function editAction()
    {
        $id = $this->request->getQuery("id", "int", "-1");
		
        $this->composeWithEdit(true);

        if($id < 1)
        {
            return $this->response->redirect('campaigns/compose');
        }

        $currentUser = $this->auth->getUser();
        if (true || !$this->request->isPost()) 
        {
            $campaign = Campaigns::getCampaignByIdForUser($id,$currentUser);
            if ($campaign == null) 
            {
                return $this->response->redirect('campaigns/compose');
            }
            // $campaign = Campaigns::findFirstById($id);
            $this->view->form->bind(array(
                    'from-name'     => $campaign->fromName,
                    'text'          => $campaign->text,
                    'schedule-date' => $campaign->date,
                    'event-time'    => $campaign->time,
                    'send-every'    => $campaign->sendEveryCount
                ), $campaign
            );

            $repeatReminderCount = $campaign->repeatReminderCount;
            $this->view->form->get('repeat-reminder-select')->setDefault($repeatReminderCount . ($repeatReminderCount>1?"times":"time"));
            $this->view->form->get('time-zone-select')->setDefault($campaign->timeZone);

            $this->view->sendEveryRadioChecked = $campaign->sendEveryIntervalUnit;
            $this->view->checkedClubIds = array(
                $campaign->clubsId => true
            );

            $this->view->campaign = $campaign;
        }
        else if (!$this->request->isPost())
        {
            $this->view->timezoneIndex = -1;
        }

        $this->view->templates = Templates::getTemplatesForUser($currentUser);//findByCreatedBy($this->auth->getUser()->id);
    }

    /**
     * Common function for composing and editing campaign;
     * If $isEditing is true this acts as campaign editing function
     * else it acts as campaign composing function
     */
    private function composeWithEdit($isEditing)
    {
        $eCampaign = null;
        $groupedClubs = null;
        if ($isEditing) 
        {
            $id = $this->request->getQuery("id", "int", "-1");
            
            $currentUser = $this->auth->getUser();
            $eCampaign = Campaigns::getCampaignByIdForUser($id,$currentUser);
			

            if ($eCampaign == null) 
            {
                return $this->response->redirect('campaigns/compose');
            }
            else
            {
                $this->view->eCampaign = $eCampaign;
				
				$this->view->currentCampaignId = $id;
                
                $eCampaignClub = $eCampaign->clubs;

                $groupedClubs = array($eCampaignClub->organizationsId=>array($eCampaignClub));
            }
        }

        $form = new CampaignsForm();
        $this->view->form = $form;
        
        $saved = true;

        if ($this->request->isPost()) 
        {	
		
			$deleteSubmit = $this->request->getPost('delete-campaign-submit');
            if (isset($deleteSubmit)) 
            {
                return $this->dispatcher->forward( array(
                    'controller' => 'campaigns',
                    'action' => 'deletecurrent',
                    'params' => array('type' => $deleteSubmit)
                ));
            }

            $this->view->sendEveryRadioChecked = $this->request->getPost('send-every-radio','string');

            $affectedClubs = array();
            $smsPerClubCountDiff = 0;
            $mmsPerClubCountDiff = 0;
            $subscribersArray = array();

            $fromName = $this->request->getPost('from-name', 'string');
            $text = $this->request->getPost('text');
            $templatesContentsIds = $this->request->getPost('new-upload-template-contents','string');
            
            $templatesContentsArray = TemplatesContents::query()
                ->inWhere('id',$templatesContentsIds)
                ->execute();
            
            $this->view->templatesContentsArray = $templatesContentsArray;

            $createTemplateName = $this->request->getPost('create-template-name','string');
            $this->view->creatingTemplate = isset($createTemplateName);
            
            try
            {
                if (isset($createTemplateName)) 
                {
                    $newTemplate = new Templates();
                    $newTemplate->name = $createTemplateName;
                    $newTemplate->text = $text;
                    $newTemplate->fromName = $fromName;

                    if ($newTemplate->save()) 
                    {
                        $templateContents = TemplatesContents::query()
                            ->inWhere('id',$templatesContentsIds)
                            ->execute();

                        foreach ($templateContents as $templateContent) 
                        {
                            $templateContent->templatesId = $newTemplate->id;
                            $templateContent->save();
                        }

                        $this->view->selectedTemplateId = $newTemplate->id;
                        $form->get('template-select-radio')->setDefault('');
                        $form->get('create-template-name')->setDefault('');
                    }
                    $this->view->templatesContentsArray = null;
                }
            }
            catch (Exception $e)
            {
                // nothing
            }

            if($this->isValid($form))
            {   

                try 
                {
                    $clubIds = $this->request->getPost("club-ids",'string');

                    if ($eCampaign) 
                    {
                        $clubsId = [$eCampaign->clubsId];
                    }
                    
                    $clubIdsCount = count($clubIds);
                    
                    $textLength = strlen($text);

                    if ($textLength > 1600) 
                    {
                        throw new Exception("Text message cannot be greater than 1600 characters", 1);
                    }
                    $segments = (int) ( ($textLength == 1600) ? 10 : ( ($textLength / 160) + 1) );

                    if($clubIdsCount < 1)
                    {
                        throw new ClubSelectionException("Please select at least one club", 1);
                    }
                    
                    $clubs = Clubs::query()
                                  ->inWhere("id",$clubIds)
                                  ->andWhere("active=".Constants::CLUB_ACTIVE_YES)
                                  ->execute();

                    if(count($clubs) < 1)
                    {
                        throw new Exception("No club found", 1);
                    }

                    $hasFiles = $this->request->hasFiles();
                    $filePath = null;

                    $templatesContentsId = null;
                    if (!$hasFiles) 
                    {
                        $templatesContentsId = $this->request->getPost('template-content-radio','int');

                        if(isset($templatesContentsId))
                        {
                            $templatesContents = TemplatesContents::findFirstById($templatesContentsId);
                            if ($templatesContents) 
                            {
                                $filePath = $templatesContents->imagePath;
                            }
                            else
                            {
                                throw new Exception(Constants::FLASH_MSG_PART_OOOPS." Selected template content does not exist.", 1);
                            }
                        }
                    }
                    $scheduleCheck = $this->request->getPost('compose-submit-schedule','string');
                    
                    $scheduleDate = null;
                    $eventTime = null;
                    $timeZone = null;
                    $repeatReminderCount = null;
                    $sendEveryCount = null;
                    $sendEveryIntervalUnit = null;

                    if (isset($scheduleCheck)) 
                    {
                        $scheduleDate = $this->request->getPost('schedule-date','string');
                        $eventTime = $this->request->getPost('event-time','string');
                        $timeZone = $this->request->getPost('time-zone-select','string');
                        $repeatReminderCount = 1;//$this->request->getPost('repeat-reminder-select','string');
                        $sendEveryCount = 1;//$this->request->getPost('send-every','string');
                        $sendEveryIntervalUnit = Constants::TIME_INTERVAL_UNIT_SHORT_MINUTE;//$this->request->getPost('send-every-radio','string');
                    }


                    $messagesPerClubCount = ($scheduleCheck ? $repeatReminderCount : 1) * $segments;
                    $isMms = $hasFiles || ($filePath != null);

                    $smsPerClubCountDiff = ($messagesPerClubCount) + ($isMms ? 1 : 0);
                    $mmsPerClubCountDiff = ($isMms ? 1 : 0);

                    $subscribersByClubsIds = array();

                    if (!$isEditing) 
                    {
                        $clubsCountByOrganizationsIds = array();
                        $subscribersCountsByOrganizationsIds = array();
                        $subscribersCountsByClubsIds = array();
                        // get subscribers count of clubs of organizations
                        foreach ($clubs as $club) 
                        {
                            $subscribers = Subscribers::getSubscribersForClub($club);
                            $subscribersCount = count($subscribers);

                            if ($subscribersCount == 0) 
                            {
                                throw new Exception(Constants::FLASH_MSG_PART_OOOPS." \"".$club->name."\" has no subscribers.", 1);
                            }

                            $subscribersCountsByClubsIds[$club->id] = $subscribersCount;

                            $key = $club->organizationsId;
                            if (!isset($subscribersCountsByOrganizationsIds[$key])) 
                            {
                                $subscribersCountsByOrganizationsIds[$key] = 0;
                            }
                            
                            if (!isset($clubsCountByOrganizationsIds[$key])) 
                            {
                                $clubsCountByOrganizationsIds[$key] = 1;
                            }
                            else
                            {
                                $clubsCountByOrganizationsIds[$key] += 1;
                            }

                            $subscribersCountsByOrganizationsIds[$key] += $subscribersCount;

                            $subscribersByClubsIds[$club->id] = $subscribers;
                        }

                        // Make sure limits can be deducted
                        foreach ($subscribersCountsByOrganizationsIds as $key => $count) 
                        {
                            $mmsCount = $clubsCountByOrganizationsIds[$key];
                            $organization = Organizations::findFirstById($key);
                            $organization->assertCanDeductLimits($smsPerClubCountDiff*$count,$mmsPerClubCountDiff*$mmsCount);
                        }

                        // Deduct limits
                        foreach ($subscribersCountsByOrganizationsIds as $key => $count) 
                        {
                            $mmsCount = $clubsCountByOrganizationsIds[$key];
                            Organizations::deductSmsAndMmsLimitsFromOrganizationWithId($key,$smsPerClubCountDiff*$count,$mmsPerClubCountDiff*$mmsCount,false,false,true);
                        }

                        foreach ($clubs as $club) 
                        {
                            $club->smsPerMonthLimitUsed += $smsPerClubCountDiff * $subscribersCountsByClubsIds[$club->id];
                            $club->mmsPerMonthLimitUsed += $mmsPerClubCountDiff * 1; //$subscribersCountsByClubsIds[$club->id];
                            $club->save();
                        }
                    }

                    $clubsCount = count($clubs);
                    if ($clubIdsCount != $clubsCount) 
                    {
                        throw new Exception("Invalid selection", 1);
                    }
                    $messageText = $fromName . " " . $text;
                    $messageContents = array(SubscribersMessagesContents::createWithTextAndImagePath($messageText,null));
                    if ($filePath) 
                    {
                        $messageContents[] = SubscribersMessagesContents::createWithTextAndImagePath("",$filePath);
                    }
                    
                    if ($messageContents == null) 
                    {
                        throw new Exception("Failed to create message content", 1);
                    }

                    if ($hasFiles == true) 
                    {
                        $postFile = $this->request->getUploadedFiles()[0];
                        
                        $file = Files::createNew($postFile);
                        $filePath = $file->path;
                    }    
                    
                    $lastCampaign = null;
                    for ($i=0; $i < $clubsCount; $i++) 
                    { 
                        $club = $clubs[$i];

                        $campaign = null;

                        if($isEditing)
                        {
                            $id = $this->request->getQuery("id", "int", "-1");
                            if($id > 0)
                            {
                                $campaign = Campaigns::findFirstById($id);
                            }
                        }
                        
                        if (!isset($campaign))
                        {
                            $campaign = new Campaigns();
                        }
                        
                        $campaign->fromName = $fromName;
                        $campaign->text = $text;
                        $campaign->imagePath = $filePath;
                        $campaign->clubsId = $club->id;

                        $campaign->templatesContentsId = $templatesContentsId;

                        $subscribers = Subscribers::getSubscribersForClub($club);

                        if (isset($scheduleCheck)) 
                        {
                            $campaign->scheduled = 1;

                            $campaign->date = $scheduleDate;
                            $campaign->time = $eventTime;
                            $campaign->timeZone = $timeZone;
                            $campaign->repeatReminderCount = $repeatReminderCount;
                            $campaign->sendEveryCount = ($sendEveryCount ? $sendEveryCount : 1);
                            $campaign->sendEveryIntervalUnit = $sendEveryIntervalUnit;
                        }
                        else
                        {
                            $timestamp = strtotime('now');

                            $date = date(Constants::DATE_FORMAT,$timestamp);
                            $time = date(Constants::TIME_FORMAT,$timestamp);

                            if (!$isEditing)
                            {
                                SubscribersMessages::bulkInsert($subscribers,$messageContents[0]->id,Constants::MESSAGE_TYPE_OUTBOX,$club->id);
                                if (count($messageContents) > 1) 
                                {
                                    sleep(1);
                                    SubscribersMessages::bulkInsert($subscribers,$messageContents[1]->id,Constants::MESSAGE_TYPE_OUTBOX,$club->id);
                                }
                            }

                            $campaign->date = $date;
                            $campaign->time = $time;
                        }

                        // Counting how many times campaign messages are sent for a club
                        $club = $campaign->clubs;
                        $club->campaignsCounter = $club->campaignsCounter + 1;
                        $club->update();
                        
                        if (!$campaign->save()) 
                        {
                            $this->flash->error($campaign->getMessages());
                            $saved = false;
                            break;
                        }

                        if (!$isEditing) 
                        {
                            CampaignsSubscribers::bulkInsert($subscribers,$campaign->id);
                        }

                        if ($lastCampaign) 
                        {
                            $campaign->relatedTo = $lastCampaign->id;
                        }
                        else
                        {
                            if ($clubsCount > 1) 
                            {
                                $campaign->relatedTo = $campaign->id;
                            }
                            else
                            {
                                $campaign->relatedTo = null;
                            }
                        }
                        $campaign->save();

                        $lastCampaign = $campaign;
                    }

                    if ($saved) 
                    {
                        $composedOrEditd = null;

                        if($isEditing)
                        {
                            $composedOrEditd = "edited";
                        } 
                        else
                        {
                            $composedOrEditd = "composed";

                            Tag::resetInput();
                            $this->view->sendEveryRadioChecked = Constants::TIME_INTERVAL_UNIT_SHORT_MINUTE;
                        }

                        $successMessage = Constants::FLASH_MSG_PART_SUCCESS." Your message was ";
                        if ($scheduleCheck) 
                        {
                            $successMessage .= "scheduled!";
                        }
                        else
                        {
                            $successMessage .= "sent!";
                        }

                        $this->flash->success($successMessage);
                        $this->view->hideForm = true;
                    }
                } 
                catch (ClubSelectionException $e)
                {
                    $saved = false;

                    $this->showFormRequestErrorMessage();
                    $form->setPriorityMessageFor('club-ids[]', $e->getMessage());
                }
                catch (Exception $e)
                {
                    $saved = false;

                    $this->flash->error($e->getMessage());
                }
            }
            else
            {
                $saved = false;
            }

            $timezoneSelect = $this->request->getPost('time-zone-select');
            if (isset($timezoneSelect)) 
            {
                $this->view->form->get('time-zone-select')->setDefault($timezoneSelect);
            }
        }
        else
        {
            $this->view->sendEveryRadioChecked = Constants::TIME_INTERVAL_UNIT_SHORT_MINUTE;
        }

        $user = $this->auth->getUser();

        $this->view->form = $form;
        $this->view->groupedClubs = $groupedClubs!=null ? $groupedClubs : Clubs::getGroupsByOrganizationsIdForUser($user);
        $this->view->groupedTemplatesContents = TemplatesContents::getGroupsByTemplatesId();
        $this->view->isEditing = $isEditing;

        $checkedClubIds = array();
        
        if($isEditing || !$saved)
        {
            
            $clubIds = $this->request->getPost("club-ids",'string');
            $clubIdsCount = count($clubIds);

            for ($i=0; $i < $clubIdsCount; $i++) 
            { 
                $clubId = $clubIds[$i];
                $checkedClubIds[$clubId]=true;
            }
        }

        $this->view->checkedClubIds = $checkedClubIds;
    }
}
