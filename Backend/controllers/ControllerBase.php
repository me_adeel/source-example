<?php
namespace Text\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

use Text\Forms\SearchForm;
use Text\Common\Constants;
use Text\Models\Emails;

/**
 * ControllerBase
 * This is the base controller for all controllers in the application
 */
class ControllerBase extends Controller
{
    public $hasAssociatedClubs = false;

    /**
     * Initialization common to all controllers 
     * that are dervied from this controller
     */
    public function initialize() 
    {
        $user = $this->auth->getUser();
        $this->view->setVar('loggedInUser', $user);
        $this->user = $user;
        if ($user) 
        {
            $this->view->isAdmin = $user->isSuperAdmin();
            $this->view->canInviteSubscribers = $user->isSuperAdmin();

            $this->view->emailsCount = Emails::count(array(
                "conditions" => "userIdTo='$user->id' AND deleted='.Constants::DELETED_NO.' AND hasRead='Constants::HAS_READ_NO'"
            ));
        }

        $searchForm = new SearchForm();
        $this->view->searchForm = $searchForm;

        $controllerName = $this->dispatcher->getControllerName();
        $actionName = $this->dispatcher->getActionName();

        $this->view->controllerName = $controllerName;
        $this->view->actionName = $actionName;
        
        $this->flashSession->output();

        $this->view->SMS = Constants::MESSAGE_TYPE_SHORT_SMS;
        $this->view->MMS = Constants::MESSAGE_TYPE_SHORT_MMS;
    }

    /**
     * Execute before the router so we can determine if this 
     * is a private controller, and must be authenticated, or a
     * public controller that is open to all.
     *
     * @param Dispatcher $dispatcher
     * @return boolean
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));

        $controllerName = $dispatcher->getControllerName();

        // Only check permissions on private controllers
        if ($this->acl->isPrivate($controllerName)) {

            // Get the current identity
            $identity = $this->auth->getIdentity();

            // If there is no identity available the user is redirected to index/index
            if (!is_array($identity)) {

                $dispatcher->forward(array(
                    'controller' => 'session',
                    'action' => 'login'
                ));

                return false;
            }

            $user = $this->auth->getUser();
            // Check if the user have permission to the current option
            $actionName = $dispatcher->getActionName();

            if (!$this->acl->isAllowed($identity['rolesId'], $controllerName, $actionName) 
                || !$this->acl->isUserAllowed($user,$controllerName,$actionName)) {

                $this->flash->notice(Constants::FLASH_MSG_PART_OOOPS.' You do not have access to this feature.');// . $controllerName . ':' . $actionName);

                if ($this->acl->isAllowed($identity['rolesId'], $controllerName, 'index')) {
                    $dispatcher->forward(array(
                        'controller' => 'index',
                        'action' => 'blank'
                    ));
                } else {
                    $dispatcher->forward(array(
                        'controller' => 'index',
                        'action' => 'blank'
                    ));
                }

                return false;
            }
        }

        try 
        {
            $user = $this->auth->getUser();
            
            if ($user) 
            {
                $this->hasAssociatedClubs = $user->hasAssociatedClubs();
                $this->hasAssociatedActiveClubs = $user->hasAssociatedActiveClubs();

                $this->view->setVar('has_associated_clubs', $this->hasAssociatedClubs);
                $this->view->setVar('has_associated_active_clubs', $this->hasAssociatedActiveClubs);
            }
        }
        catch (\Exception $e)
        {
            $this->flash->error($e);
        }
    }

    /**
     * Shows error alert with generic error message
     * defined in Constants::ERROR_MSG_FORM_REQUEST
     */
    public function showFormRequestErrorMessage()
    {
        $this->flash->error(Constants::ERROR_MSG_FORM_REQUEST);
    }

    /**
     * Checks if the form is valid and returns true
     * else prints error message or $defaultMessage if not null
     *
     * @param \Phalcon\Forms\Form $form
     * @param string $defaultMessage
     *
     * @return boolean
     */    
    public function isValid($form, $defaultMessage = null)
    {
        if($form->isValid($this->request->getPost()) != false)
        {
            return true;
        }
        else
        {
            if ($defaultMessage != null) 
            {
                $this->flash->error($defaultMessage);
            }
            else
            {
                $this->showFormRequestErrorMessage();
            }
            // for debugging purpose
            // foreach($form->getMessages() as $message) 
            // {
            //     $this->flash->error($message);
            // }
        }

        return false;
    }

    /**
     * Checks if the form is valid and returns true
     * else if redirect to same page if from has csrf error
     * else prints error message or $defaultMessage if not null
     *
     * @param \Phalcon\Forms\Form $form
     * @param string $defaultMessage
     *
     * @return boolean
     */    
    public function isValidWithCSRF($form, $defaultMessage = null)
    {
        if($form->isValid($this->request->getPost()) != false)
        {
            return true;
        }
        else
        {
            foreach ($form->getMessages() as $message) 
            {
                if ($message->getField() == 'csrf')
                {
                    $redirection = $this->dispatcher->getControllerName() . '/' . $this->dispatcher->getActionName();

                    return $this->response->redirect($redirection);
                }
            }
            
            if ($defaultMessage != null) 
            {
                $this->flash->error($defaultMessage);
            }
            else
            {
                $this->showFormRequestErrorMessage();
            }
        }

        return false;
    }
}
