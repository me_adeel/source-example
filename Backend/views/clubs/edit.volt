        <form method="post" role="form" enctype="multipart/form-data">
            {{ form.render('club-id',{'id':'club-id'}) }}
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="page-header club-add-logo-btn"> <span class="pull-left">Edit Club</span>
                        <div class="pull-right">
                            <span class="btn btn-file" title="Minimum size: 400px X 400px" style="padding: 5px">
                                <label for="upload-logo"><span><span class="glyphicon glyphicon-cloud-upload"></span> Add a Logo</span></label>
                                {{ form.render('upload-logo',{'class':'hidden'}) }}
                                <img src="{{url.get(club.getLogoPath())}}">
                            </span>
                        </div>
                        <div class="clearfix"></div>
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    {{content()}}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        {{ formSubmit.label('club-name') }}
                                        {{ formSubmit.render('club-name',{'class':'form-control'}) }}
                                        <div class="messages">{{ formSubmit.messagesSmall('club-name') }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        {{ formSubmit.label('club-url') }}
                                        {{ formSubmit.render('club-url',{'class':'form-control url-field'}) }}
                                        <div class="messages">{{ formSubmit.messagesSmall('club-url') }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        {{ formSubmit.label('phone') }}
                                        {{ formSubmit.render('phone',{'class':'form-control'}) }}
                                        <div class="messages">{{ formSubmit.messagesSmall('phone') }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        {{ formSubmit.label('mailing-address-1') }}
                                        {{ formSubmit.render('mailing-address-1',{'class':'form-control'}) }}
                                        <div class="messages">{{ formSubmit.messagesSmall('mailing-address-1') }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        {{ formSubmit.label('mailing-address-2') }}
                                        {{ formSubmit.render('mailing-address-2',{'class':'form-control'}) }}
                                        <div class="messages">{{ formSubmit.messagesSmall('mailing-address-2') }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        {{ formSubmit.label('mailing-address-3') }}
                                        {{ formSubmit.render('mailing-address-3',{'class':'form-control'}) }}
                                        <div class="messages">{{ formSubmit.messagesSmall('mailing-address-3') }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        {{ formSubmit.label('club-city') }}
                                        {{ formSubmit.render('club-city',{'class':'form-control'}) }}
                                        <div class="messages">{{ formSubmit.messagesSmall('club-city') }}</div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {{ formSubmit.label('club-state') }}
                                        <div class="selectbox-wrapper">
										{{ formSubmit.render('club-state',{'class':'form-control'}) }}
                                        </div>
										<div class="messages">{{ formSubmit.messagesSmall('club-state') }}</div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {{ formSubmit.label('club-zip') }}
                                        {{ formSubmit.render('club-zip',{'class':'form-control'}) }}
                                        <div class="messages">{{ formSubmit.messagesSmall('club-zip') }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                {{ formSubmit.label('club-keyword') }}
                                {{ formSubmit.render('club-keyword',{'class':'form-control club-add-club-keyword','data-original':originalKeyword}) }}
                                <div class="messages">{{ formSubmit.messagesSmall('club-keyword') }}</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                {{ formSubmit.label('club-welcome-message') }}
                                {{ formSubmit.render('club-welcome-message',{'class':'form-control','disabled':canEditClubWelcomeMessage?null:'disabled'}) }}
                                <div style="font-size:.85em">Maximum message size of 160 characters. (<span class="welcome-message-length-used">0</span> used)</div>
                                <div class="messages">{{ formSubmit.messagesSmall('club-welcome-message') }}</div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                            {% if canEditClubActive %}
                                {{ form.render('club-activate-check',{'checked':(club.active?'checked':null)}) }}
                                {{ form.label('club-activate-check') }}
                            {% endif %}
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <a href="javascript:;"><input type="button" value="Cancel" class="btn btn-default form-control" onclick="window.history.go(-1); return false;"></a>
                                        </div>
                                        <div class="col-sm-6">
                                            {{ form.render('finish-edit-club-submit', {'class':'form-control btn btn-success','value':'Update'}) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-header sub">
                                    <span>Users (optional)</span>
                                    <span class="pull-right">
                                        <a href="" onclick="return false" data-toggle="modal" data-target="#club-add-user">Add New +</a>
                                    </span>
                                </h4>
                            </div>
                        </div>

                        {% set clubOwners = club.getClubsOwners() %}
                        {% set clubOwnersCount = clubOwners|length %}
                        {% if clubOwnersCount > 0 %}<strong>Owner{{ clubOwnersCount > 1 ? 's' : '' }}</strong>{% endif %}
                        {% for owner in clubOwners %}
                        <div class="row users-list">
                            <div class="col-sm-12">
                                <div class="users-list-item">
                                    {% if loggedInUser.canRemoveClubUsers() %}
                                    <div class="item-col">
                                        <a href="{{url.get('clubs/removeUser/'~club.id~'/'~owner.id)}}">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </a>
                                    </div>
                                    {% endif %}
									{# club owner details bottom #}
                                    <div class="item-col">
										<p class="pad-bw-elements">
										{# canViewProfile allows only super admin to view the profile or an organization owner itself #}
										<a {% if canViewProfile %} href="{{url.get('users/view?id='~owner.id)}}" {% else %} href="javascript:;" {% endif %}>{{ owner.firstName }} {{ owner.lastName }}</a>
											, <a href="mailto:{{ owner.email }}">{{ owner.email }}</a>
											, <a class="phone-text" href="tel:{{ owner.phone }}">{{ owner.phone }}</a>
										</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {% endfor %}
                        {% set clubManagers = club.getClubsManagers() %}
                        {% set clubManagersCount = clubManagers|length %}
                        {% if clubManagersCount > 0 %}<strong>Manager{{ clubManagersCount > 1 ? 's' : '' }}</strong>{% endif %}
                        {% for manager in clubManagers %}
                        <div class="row users-list">
                            <div class="col-sm-12">
                                <div class="users-list-item">
                                    {% if canAddRemoveUsers %}
                                    <div class="item-col">
                                        <a href="{{url.get('clubs/removeUser/'~club.id~'/'~manager.id)}}">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </a>
                                    </div>
                                    {% endif %}
                                    <div class="item-col">
                                        <p class="pad-bw-elements">
                                        <a {% if canViewProfile %}href="{{url.get('users/view?id='~manager.id)}}"{%else%}href="javascript:;"{% endif %}>{{ manager.firstName }} {{ manager.lastName }}
                                        , <a href="mailto:{{ manager.email }}">{{ manager.email }}</a>
                                        , <a class="phone-text" href="tel:{{ manager.phone }}">{{ manager.phone }}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {% endfor %}

                </div>
            </div>
        </form>

<div class="modal fade" id="club-add-user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="club-add-user" role="form" action="{{url.get('clubs/json_addUser')}}" method="post" data-use-ajax="true" id="club-add-user-form">
            <!--<form method="post" class="club-add-user" role="form">-->
                {{ clubUserForm.render('club-id') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 class="modal-title" id="myModalLabel">Add User <span class="ajax-loading icon-loading"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-12 main-messages">
                        </div>
                    </div>
                    <div class="form-group">
                        {{ clubUserForm.render('user-role-select', {'class':'form-control'} )}}
                        <div class="messages"></div>
                    </div>
                    <div class="form-group">
                        {{ clubUserForm.label('user-first-name') }}
                        {{ clubUserForm.render('user-first-name', {'class':'form-control'} )}}
                        <div class="messages"></div>
                    </div>
                    <div class="form-group">
                        {{ clubUserForm.label('user-last-name') }}
                        {{ clubUserForm.render('user-last-name', {'class':'form-control'} )}}
                        <div class="messages"></div>
                    </div>
                    <div class="form-group">
                        {{ clubUserForm.label('user-email') }}
                        {{ clubUserForm.render('user-email', {'class':'form-control'} )}}
                        <div class="messages"></div>
                    </div>
                    <div class="form-group">
                        {{ clubUserForm.label('user-phone') }}
                        {{ clubUserForm.render('user-phone', {'class':'form-control'} )}}
                        <div class="messages"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="form-control btn btn-default" data-dismiss="modal" value="Cancel">
                    {{ submit_button('Done','name':'add-club-user-submit','class':'form-control btn btn-success') }}
                </div>
            </form>
        </div>
    </div>
</div>