        <form action="{{url.get('clubs/add?orgid='~organization.id~(step?('&step='~step):''))}}" method="post" role="form" enctype="multipart/form-data">
            {{ form.render('organization-id') }}
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="page-header club-add-logo-btn"> <span class="pull-left">{{step?'Step 2: ':''}}Add Club{{step?' in '~organization.name:''}}</span>
                        <div class="pull-right">
                            <span class="btn btn-file" title="Minimum size: 400px X 400px">
                                <label for="upload-logo"><span><span class="glyphicon glyphicon-cloud-upload"></span> Add a Logo</span></label>
                                {{ form.render('upload-logo') }}
                            </span>
                        </div>
                        <div class="clearfix"></div>
                    </h2>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    {{content()}}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form role="form" class="club-all-forms">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        {{ form.label('club-name') }}
                                        {{ form.render('club-name',{'class':'form-control'}) }}
                                        <div class="messages">{{ form.messagesSmall('club-name') }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        {{ form.label('club-url') }}
                                        {{ form.render('club-url',{'class':'form-control url-field'}) }}
                                        <div class="messages">{{ form.messagesSmall('club-url') }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        {{ form.label('phone') }}
                                        {{ form.render('phone',{'class':'form-control'}) }}
                                        <div class="messages">{{ form.messagesSmall('phone') }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        {{ form.label('mailing-address-1') }}
                                        {{ form.render('mailing-address-1',{'class':'form-control'}) }}
                                        <div class="messages">{{ form.messagesSmall('mailing-address-1') }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        {{ form.label('mailing-address-2') }}
                                        {{ form.render('mailing-address-2',{'class':'form-control'}) }}
                                        <div class="messages">{{ form.messagesSmall('mailing-address-2') }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        {{ form.label('mailing-address-3') }}
                                        {{ form.render('mailing-address-3',{'class':'form-control'}) }}
                                        <div class="messages">{{ form.messagesSmall('mailing-address-3') }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        {{ form.label('club-city') }}
                                        {{ form.render('club-city',{'class':'form-control'}) }}
                                        <div class="messages">{{ form.messagesSmall('club-city') }}</div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {{ form.label('club-state') }}
										<div class="selectbox-wrapper">
                                        {{ form.render('club-state',{'class':'form-control'}) }}
                                        </div>
										<div class="messages">{{ form.messagesSmall('club-state') }}</div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {{ form.label('club-zip') }}
                                        {{ form.render('club-zip',{'class':'form-control'}) }}
                                        <div class="messages">{{ form.messagesSmall('club-zip') }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                {{ form.label('club-keyword') }}
                                {{ form.render('club-keyword',{'class':'form-control club-add-club-keyword','data-original':''}) }}
                                <div class="messages">{{ form.messagesSmall('club-keyword') }}</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                {{ form.label('club-welcome-message') }}
                                {{ form.render('club-welcome-message',{'class':'form-control'}) }}
                                <div style="font-size:.85em">Maximum message size of 160 characters. (<span class="welcome-message-length-used">0</span> used)</div>
                                <div class="messages">{{ form.messagesSmall('club-welcome-message') }}</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-header sub">Owner (optional)
                                </h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    {{ form.label('owner-first-name') }}
                                    {{ form.render('owner-first-name',{'class':'form-control'}) }}
                                    <div class="messages">{{ form.messagesSmall('owner-first-name') }}</div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    {{ form.label('owner-last-name') }}
                                    {{ form.render('owner-last-name',{'class':'form-control'}) }}
                                    <div class="messages">{{ form.messagesSmall('owner-last-name') }}</div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    {{ form.label('owner-email') }}
                                    {{ form.render('owner-email',{'class':'form-control'}) }}
                                    <div class="messages">{{ form.messagesSmall('owner-email') }}</div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    {{ form.label('owner-phone') }}
                                    {{ form.render('owner-phone',{'class':'form-control phone-field', 'id':null}) }}
                                    <div class="messages">{{ form.messagesSmall('owner-phone') }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="users-list">

                        </div>
                        <div class="club-submit-step">
                            <div class="row">
                                <div class="col-sm-9">
                                </div>
                                <div class="col-sm-3">
                                    {{ form.render('finish-add-club-submit', {'class':'form-control btn btn-success'}) }}
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </form>
