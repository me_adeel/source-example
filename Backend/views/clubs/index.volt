<div class="row">
    <div class="col-sm-12">
        <h3 class="page-header">
            <span>Clubs</span>
        </h3>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        {{content()}}
    </div>
</div>

        <div class="row">
            <div id="clubs-myCarousel" class="carousel slide" data-ride="carousel"> 
                <!-- Indicators -->
                
                <?php $slidesCount = ceil(count($clubs) / $each) ?>
                {% if slidesCount > 1 %}
                <ol class="carousel-indicators">
                    <?php for($y=0; $y<$slidesCount; $y++) { ?>
                    <li data-target="#clubs-myCarousel" data-slide-to="<?php echo $y;?>" <?php echo $y == 0 ? 'class="active"' : ''; ?>></li>
                    <?php } ?>
                </ol>
                {% endif %}

                <div class="carousel-inner" role="listbox">

                    {% for key, club in clubs %} 

                        {% if loop.first %}
                            {{ '<div class="item active">' }}
                        {% elseif (key % each) == 0 %}
                            {{ '<div class="item">' }}
                        {% endif %}
                        
                        <div class="col-sm-3 club-cards-dashboard">
                            <div class="panel {{club.active?'panel-primary':'panel-red'}}">
                                <div class="panel-heading">
                                    <div class="row">
                                        <a href="{{url.get('clubs/edit?id='~club.id)}}">
                                            <div class="col-xs-12 text-org-card">
                                                <h4>{{club.name}}</h4>
                                                {% set owners = club.getClubsOwners() %}
                                                {% for owner in owners %}
                                                <p class="small">{{owner.getFullName()}}</p>
                                                <p class="small phone-text">{{owner.phone}}</p>
                                                {% break %}
                                                {% endfor %}
                                                <p class="small">Text Messages &nbsp;<span>{{club.smsPerMonthLimitUsed}}/{{club.smsPerMonthLimit}}</span> per month</p>
                                                <p class="small">Images &nbsp;<span>{{club.mmsPerMonthLimitUsed}}/{{club.mmsPerMonthLimit}}</span> per month</p>
                                                
                                                {% set managers = club.getClubsManagers() %}
                                                {% set managersCount = managers|length %}
                                                {% set usersCount = managersCount + (owners|length) %}
                                                <p class="small">Managers &nbsp;<span>{{managersCount}}</span></p>
                                                <p class="small">Users &nbsp;<span>{{usersCount}}</span></p>

                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="{{url.get('clubs/edit?id='~club.id)}}"><span class="pull-left">View Details</span> <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                    </a>
                                </div>
                            </div>
                        
                        </div>

                        {% set i = key+1 %}
                        {% if loop.first == false and (i % each) == 0 %}
                            {{ '</div>' }}
                        {% endif %}
                    {% endfor %}

                    {% if ((clubs|length) % each) != 0 %}
                        {{ '</div>' }}
                    {% endif %}
                </div>
            </div>
        </div>

        {% set paginatorData = [
            'page':page,
            'append':'#page-wrapper'
        ] %}
        {% include "partials/paginator" %}
