
{% set hasItems = page.items|length > 0 %}
<form method="post">
<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">{% block block_header_name %}Campaigns{% endblock %}</h2>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        {{content()}}
    </div>
</div>
<div class="row">
    {% block block_calendar_list_type %}
    <div class="col-sm-12">
        <h3 class="small email-inbox-anc">
            {{link_to("campaigns/upcoming",
                      '<span class="glyphicon glyphicon-calendar"></span>&nbsp; Upcoming Campaigns',
                      'class': actionName == 'upcoming' ? 'active' : '',
                      'data-link-hash': 'page-wrapper')}}
            {{link_to("campaigns/past",
                      '<span class="glyphicon glyphicon-calendar"></span>&nbsp; Past Campaigns',
                      'class': actionName == 'past' ? 'active' : '',
                      'data-link-hash': 'page-wrapper')}}
            

            <a data-link-hash="page-wrapper" class="link-for-btn" href="" onclick="return false" data-btn-id="delete-campaign-submit"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp; Delete</a>
            <div class="row hidden">
                {{ submit_button("Delete", "class": "btn btn-success", "id":"delete-campaign-submit", "name":"delete-campaign-submit", "value":actionName) }}
            </div>

        </h3>
    </div>
    {% endblock %}
    <div class="col-sm-12">
        <div class="table-responsive">

            {% block listLoopBlock %}
            {% set currentGroupDate = null %}
            {% if page.items|length > 0 %}
            {% for campaign in page.items %}
                {% set newDate = (currentGroupDate != campaign.date) %}
                {% if (newDate) %}
                {% set currentGroupDate = campaign.date %}
                
                    {% if !loop.first %}
                    </tbody>
                </table>
                    {% endif %}
                <h4>{{ currentGroupDate }}</h4>
                <table class="table table-striped table-hover">
                    <tbody>
                {% endif %}
                        <tr class="small">
                            <td title="Club Name" width="180">{{form.render('campaign-ids[]',{'value':campaign.id})}} &nbsp;{{ campaign.clubs.name }}</td>
                            <td title="From" width="120">{{ campaign.fromName }}</td>
                            <td title="Text" width="430">{{ campaign.text }}</td>
                            <td title="Type" width="70" style="text-align: center;">{{ campaign.getMessageType() }}</td>
                            <td title="Time" width="120">{{ campaign.time }} ({{ campaign.timeZone }})</td>
                            {% if actionName == 'upcoming' %}
                            <td>{{ link_to("campaigns/edit?id=" ~ campaign.id, '<span class="glyphicon glyphicon-edit"></span>') }}</td>
                            {% elseif actionName == 'past' %}
                            <td>{{ link_to("campaigns/compose?id=" ~ campaign.id, '<span class="glyphicon glyphicon-repeat"></span>') }}</td>
                            {% endif %}
                        </tr>
                {% if loop.last %}
                    </tbody>
                </table>
                {% endif %}
                {% else %}
            {% endfor %}
            {% endif %}
            {% endblock %}
        </div>

        {% set paginatorData = [
            'page':page,
            'append':'#page-wrapper'
        ] %}
        {% include "partials/paginator" %}
        
    </div>
</div>
</form>