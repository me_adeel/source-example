<div class="row">
    <div class="col-sm-12">
        <h2 class="page-header">Calendar</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        {{content()}}
    </div>
	<div id="script-warning" class="col-md-12">
		<div class="alert alert-danger">
                File <code>get-events.php</code> must be running.
        </div>
	</div>
	<div class="col-md-12" id="loading">
        <div class="alert alert-info">Loading! Please wait...</div>
	</div>
	<div class="col-md-12">        
		<div id='calendar'></div>
	</div>
</div>
