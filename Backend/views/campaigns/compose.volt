

<form method="post" role="form" enctype="multipart/form-data">
    {% block campaign_id_block %}
    {% endblock %}
    
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Compose</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            {{content()}}
        </div>
    </div>
    <div class="row club-compose-campaigns {{hideForm is defined ? 'hidden' : ''}}">
    
        <div class="col-sm-4">
            <div class="form-group">
                    <a class="club-compose-btns form-control tag-system" href="javascript:;" data-toggle="modal" data-target="#select-organization-campaign">
						<span class="club-select-txt">Select Clubs</span>
						<div id="select-clubs-btn" class="select-org-clubs">
                    	</div>
						<span class="glyphicon-triangle-bottom glyphicon"></span>
					</a>
					<div class="clearfix"></div>
                <div class="messages">{{ form.messagesSmall('club-ids[]') }}</div>
            </div>
            <hr>
            <div class="form-group margin0">
                <label>Message Templates</label>
                <div class="clearfix"></div>
                <div class="btn-group toggle-buttons">
                    <button class="btn btn-success {{false AND creatingTemplate?"":"active"}}" type="button" id="select-template">Select</button>
                    <button class="btn btn-success {{false AND creatingTemplate?"active":""}}" type="button" id="create-template">Create</button>
                </div>
                <div class="templates-area">
                    {% set createTemplateStyle = (false AND creatingTemplate is defined AND creatingTemplate ? 'display:inline-block' : '')%}
                    {% set selectTemplateStyle = (false AND creatingTemplate is defined AND creatingTemplate ? 'display:none' : '')%}

                    {{form.render('create-template-name',{'class':'form-control create-template','style':createTemplateStyle})}}
                    <div class="select-template" style="{{selectTemplateStyle}}">

                        <a href="javascript:;" class="club-compose-btns" id="select-template-category-data">
                            <span class="select-org-clubs select-cat-data">Select from Existing</span>
                            <span class="glyphicon-triangle-bottom glyphicon"></span>
                        </a>
                        <div class="select-template-category-data">
							<div>
								{{ form.render("template-select-radio", {"value":0, "id":("template-select-radio-"~0)} ) }}
								<label for="template-select-radio-0">None</label>
							</div>
                            {% for template in templates %}
                                <hr>
								<div>
                                    {% if selectedTemplateId is defined %}
                                    {{ form.render("template-select-radio", {"value":template.id, "id":("template-select-radio-"~template.id),"checked":(selectedTemplateId==template.id?"checked":null),"data-from-name":template.fromName,"data-text":template.text} ) }}
                                    {% else %}
                                    {{ form.render("template-select-radio", {"value":template.id, "id":("template-select-radio-"~template.id),"data-from-name":template.fromName,"data-text":template.text} ) }}
                                    {% endif %}
									
									<label for="template-select-radio-{{template.id}}">{{template.name}}</label>
								</div>
                            {% endfor %}
                        </div>
                        

                        {% for templatesContentsGroup in groupedTemplatesContents %}
                            {% for templateContent in templatesContentsGroup %}

                            {% if loop.first %}
                            <div class="form-group templates-select-group row image-campaign-category" id="templates-select-group-{{templateContent.templatesId}}">
                            {% endif %}
                                <div class="col-xs-3 col-sm-3">
									<label class="template-content-item">
                                        {{ form.render("template-content-radio", {"value":templateContent.id,"id":null} ) }}
                                        <img data-src="{{url.get(templateContent.imagePath)}}" src="" alt="Template">
                                        <div class="cross-btn"></div>
									</label>
                                </div>
                            {% if loop.last%}
                            </div>
                            {% endif %}
                            {% endfor %}
                        {% endfor %}
                    </div>
                </div>
            </div>
            <div class="row" style="display:none">
                <div class="col-xs-3 col-sm-3" id="image-radio-template">
                    <label class="template-content-item">
                        <input type="hidden" name="new-upload-template-contents[]">
                        <input type="radio" name="template-content-radio" value="">
                        <img src="" alt="Template">
                        <div class="cross-btn"></div>
                    </label>
                </div>
            </div>
			<div class="row form-group image-campaign-category" id="uploaded-images-radio-container">
            
                {% if campaign.templatesContentsId is defined %}
                {% set tcid = campaign.templatesContentsId %}
                <div class="col-xs-3 col-sm-3">
                    <label class="template-content-item">
                        <input type="radio" name="template-content-radio" value="{{tcid}}">
                        <img src="{{url.get(campaign.templatesContents.imagePath)}}" alt="Template">
                        <div class="cross-btn"></div>
                    </label>
                </div>
                {% elseif templatesContentsArray is defined %}
                {% for templateContent in templatesContentsArray %}
                <div class="col-xs-3 col-sm-3">
                    <label class="template-content-item">
                        <input type="hidden" name="new-upload-template-contents[]" value="{{templateContent.id}}">
                        <input type="radio" name="template-content-radio" value="{{templateContent.id}}">
                        <img src="{{url.get(templateContent.imagePath)}}" alt="Template">
                        <div class="cross-btn"></div>
                    </label>
                </div>
                {% endfor %}
                {% endif %}
            </div>
			<hr class="margin-bottom-15">
            <div class="row form-group margin0">
                <div class="col-sm-12">
                    <div style="text-align:center" class="row">
                        <button type="button" class="btn btn-success btn-file ajax-upload-btn">Browse to Upload Image</button>
                        {{ form.render('image-file',{'class':'hidden'}) }}
                        <span class="ajax-loading icon-loading" style="display: none;"></span>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-4">
            <div class="form-group">
                {{ form.render("from-name", {"class":"form-control", "placeholder":"From"}) }}
                <div class="messages">{{ form.messagesSmall('from-name') }}</div>
            </div>
            <div class="form-group msg-remaining-letters">
                {{ form.render("text", {"class":"form-control", "placeholder":"Your Message", "id":"compose-campaign-text"}) }}
                <div class="messages">{{ form.messagesSmall('text') }}</div>
            </div>
            <div class="small usage"><i class="fa fa-fw fa-mobile"></i> <span id="remaining-characters-number">0</span>/160 (<span id="message-segments">1</span>) characters used</div>
            <div class="row">
                <div class="col-sm-6">
                    <button id="campaign-schedule-btn" class="btn btn-success" type="button" data-toggle="modal" data-target-for-modal="#event-schedule">Schedule</button>
                </div>
                <div class="col-sm-6">
                    {% if isEditing %}
                    {{ submit_button("Done", "class": "btn btn-success", "id":"compose-submit-schedule-update", "name":"compose-submit-schedule", "value":"Update") }}
                    {% else %}
                    {{ submit_button("Send", "class": "btn btn-success", "id":"compose-submit") }}
                    {% endif %}
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="iphone-sms">
                <div class="iphone-speaker">
                </div>
                <div class="iphone-screen">
                    <div class="iphone-screen-head btn-success btn">Sender</div>
                    <div class="iphone-sms-content-wrapper">
                        <div class="iphone-sms-content">
                            {% for i in 1..10 %}
                            <div class="hundred" style="display: none">
                                <div class="pull-right text-right">
                                    <img class="img" src="" style="display:none">
                                    <p class="text-left text"></p>
                                    <div>{{i}} of <span class="total-bubbles">1</span></div>
                                </div>
                                <span class="clearfix"></span>
                            </div>
                            {% endfor %}
                            <div class="hundred" style="display: none">
                                <div class="pull-right text-right">
                                    <img class="img" src="" style="display:none">
                                    <div><span class="used-bubble">{{i}}</span> of <span class="total-bubbles">1</span></div>
                                </div>
                                <span class="clearfix"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="iphone-home-btn">
                </div>
            </div> 
        </div>
    </div>

    <!-- Popups: Start -->
    {% include "partials/modal_for_compose_campaign_for_selecting_clubs" %}
    {% include "partials/modal_for_compose_campaign_for_scheduling_campaigns" %}
    <!-- Popups: End -->

</form>
